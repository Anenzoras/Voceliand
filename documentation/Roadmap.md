<!--
https://obsproject.com/forum/threads/fixed-im-looking-for-a-voice-processor.5379/ :
INPUT -> Gate (ReaGate) -> EQ (ReaEQ) -> Compressor (ReaComp) -> Limiter (Buzmaxi, but you could also use ReaComp for this) -> OUTPUT
-->

# Current active TODOs

- end "make it work" already
- Remove FIXMEs as soon as possible.
- Clean up the code a bit inside Voceliand.cpp, since it is a mess for now.
- Change AudioConfig to avoid direct configuration access and to auto compute
    bitDepth.

# Map

There are comments with a certain tag before several parts of code. Those tags
represent the degrees of badness of a code which should be rewritten when most
urgent ones are settled up.

Levels are (less bad to full bad):
- -MEH-
- -BAD-
- -DELAMERDE-

## [100%] Make it prework

- [100%] Use GLFW to open windows
- [100%] Use Imgui with OpenGl (GLEW) to display a dumb window
- [100%] Integrate libsoundio and do some testing thingies for sound
    manipulation

## [55%] Make it work (for windows)

- [75%] Display window (should have a select box for microphone device plus
    options needed for compression)
- [100%] Record microphone to a file (to test recording capabilities of audio
    lib)
    + [100%] get microphone by default (default recording device)
    + [100%] output to .wav
- [100%] Implement double recording, to prepare future tests on compression
    function
- [100%] Implement SoundFunction (or SoundEffect) as a base class, to ease
    effect chaining afterward
- [33%] Implement compression, inheriting base class (use previously
    implemented double recording to listen and estimate if algo is ok)
    + [100%] implement
    + [0%] unit test (try to test each Compressor functions one by one)
    + [0%] functionality test (listening)
- [90%] Implement output device driver (turns out we are proxying
    CoCreateInstance)
- [0%] Communicate to driver
    + send data
    + test using external recording software (Windows Recorder?)

## Make it cross-platform

(At least for Unices)

## Make it better

- Remove all -DELAMERDE- tagged code in the sources.
- Allow compression to be controlled using interface
- Seriously work on class design concerning samples: may be cool to have a
    template class
- Consider implementing high-pass filtering to remove DC offset (unsure if I
    will come accross DC offset for voice, but you never know)
- Rewrite the roadmap a bit, at this point things will be clearer

## Make it more

- Implement effect chaining
- Implement sound function : Gate (== threshold min)
    + implement
    + test
    + control
- Implement sound function : Threshold (== threshold max)
    + implement
    + test
    + control

## Make it clean

- Remove all -BAD- tagged code in the sources.
- Maybe rewrite UI code.

## Make it even more

- Implement sound function : Equalizer
    + implement
    + test
    + control
- Implement sound function : Limiter
    + implement
    + test
    + control

## Make it incredible

- Implement VST reading or equivalent
- Turn all sound functions into VST
- Allow for custom VST

## Make it fast?

- May not be needed?
- Try to remove all -MEH- tagged code in the sources.
