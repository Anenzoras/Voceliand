/*
 *  File:   AudioInputDevice.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-10
 */

#ifndef RECORD_AUDIO_INPUT_DEVICE_HPP
#define RECORD_AUDIO_INPUT_DEVICE_HPP

/* standard library includes */
#include <vector>
#include <inttypes.h>

/* external library includes */
#include <soundio/soundio.h>    // Sound device manipulation library

/* project includes */
#include "../Status.hpp"

namespace Voceliand
{
    /**
     *  @struct RecordContext
     *      Structure used to hold the reading context.
     *
     *  @property {struct SoundIoRingBuffer *} ringBuffer
     *      The reading buffer, a ring array.
     */
    struct RecordContext
    {
        struct SoundIoRingBuffer * ringBuffer;
    };

    /**
     *  @class AudioInputDevice
     *      Class used to capture audio, thanks to libsoundio library.
     *      Instancing the class sets up all the configuration needed to capture
     *      audio from any input device (a microphone).
     */
    class AudioInputDevice
    {
    public:
        /**
         *  @function AudioInputDevice
         *      Constructor. Instanciate .device and .stream.
         *      Errors may happen due to system limitations, resulting in
         *      .good being set to false, and the function returning before
         *      setting up all fields.
         *      Also starts the device captation of audio data. Do nothing if
         *      .good is false. If no error occured, the main loop can poll data
         *      using .pollSamples().
         *      If .good is ever set to false when the function returns,
         *      .pollSampleData() will do nothing and the object should be
         *      disposed of (out of scope/delete).
         */
        AudioInputDevice(bool isRaw = false, char * deviceId = NULL);

        /**
         *  @function ~AudioInputDevice
         *      Destructor. Frees up space used by the object.
         */
        ~AudioInputDevice(void);

        /**
         *  @function isGood
         *      Getter for the .good property. Whenever the function returns
         *      false, a critical error occured and the device object should be
         *      destroyed.
         */
        bool isGood(void)
        {
            return this->good;
        }

        /**
         *  @function pollSamples
         *      Poll audio samples from stream and put them in samples.
         *      This function should be called frequently.
         *
         *  @param {std::vector<char>} samples
         *      The read samples are copied in there. The array is cleared at
         *      each call. -MEH-
         */
        void pollSamples(std::vector<char> & samples);

        /**
         *  @function getFormat
         *      Returns the SoundIo format of the stream.
         *
         *  @returns {enum SoundIoFormat}
         *      The format.
         */
        enum SoundIoFormat getFormat(void) const
        {
            return this->stream->format;
        }

        /**
         *  @function getSampleRate
         *      Returns the sample rate of the .stream.
         *
         *  @returns {int}
         *      The sample rate. Possible values are 48000, 44100, 96000, 24000,
         *      0.
         */
        int getSampleRate(void) const
        {
            return this->stream->sample_rate;
        }

        /**
         *  @function getSampleSize
         *      Returns the size in bytes of one sample in the .stream.
         *
         *  @returns {uint16_t}
         *      The sample size. Should be between 1 and 8.
         */
        uint16_t getSampleSize(void) const
        {
            return this->sampleSize;
        }

        /**
         *  @function getChannelCount
         *      Returns the number of channel of the device stream.
         *
         *  @warning -MEH-
         *
         *  @returns {uint16_t}
         *      The channel count.
         */
        uint16_t getChannelCount(void) const
        {
            return this->device->current_layout.channel_count;
        }

    private:
        /**
         *  @function selectDevice
         *      Select the device in the inside the SoundIo device list.
         *
         *  @param {bool} isRaw
         *      Is used to determine if the device is raw. You most likely want
         *      it false?
         *  @param {char *} deviceId
         *      A string identifying the device. May be NULL in which case the
         *      function selects the first device available.
         *
         *  @returns {Status::Code}
         *      The status code of the function. Can return several error if a
         *      system/soundio error occured.
         */
        Status::Code selectDevice(bool isRaw, char * deviceId);

        /**
         *  @function retrieveSampleRate
         *      Choose the first possible sample rate in the internal list.
         *
         *  @returns {int}
         *      The selected sample rate.
         */
        int retrieveSampleRate(void);

        /**
         *  @function retrieveFormat
         *      Choose the first possible format for the input stream in the
         *      given list.
         *
         *  @returns {enum SoundIoFormat}
         *      The format.
         */
        enum SoundIoFormat retrieveFormat(void);

        /**
         *  @function start
         *      Start the audio captation on the device.
         *      Is called at the end of constructor function if all went
         *      correctly. If the function returns without problem, the object
         *      can be used to poll sample data from the device.
         *
         *  @returns {Status::Code}
         *      A status describing the potential error cases:
         *      FAILURE_INPUT_STREAM_UNOPENABLE if libsoundio could not open its
         *      stream;
         *      FAILURE_OUT_OF_MEMORY if libsounio ring_buffer could not be
         *      allocated;
         *      FAILURE_CANT_START_INPUT_DEVICE if libsoundio could not start
         *      the device captation;
         *      SUCCESS when everything went according to the plan. This is the
         *      only case where .good should stay true.
         */
        Status::Code start(void);

        /**
        *  @property {bool} good
        *      Information if the device is in proper state to start reading.
        *      If any soundio function called by the constructor ended in an
        *      error or incoherent state, this boolean is set to false.
        *      If this is the case, no function call can be performed on the
        *      device.
        */
        bool good;

        /**
         *  @property {SoundIoDevice *} device
         *      SoundIo structure to logically describe a sound device.
         *      Correspond to a microphone. Used by .stream.
         *      Must not be modified once .stream has been opened.
         */
        SoundIoDevice * device;

        /**
         *  @property {SoundIoInStream} stream
         *      SoundIo structure holding information regarding the input sound
         *      stream.
         *      Must be configured before calling soundio_.stream_open() and
         *      then again a bit before calling soundio_.stream_start().
         *      Must not be modified once SoundIo has successfully opened it.
         */
        SoundIoInStream * stream;

        /**
        *  @property {RecordContext} context
        *      Internal context used to read data.
        */
        RecordContext context;

        /**
         *  @property {uint16_t} sampleSize
         *      Store additional information concerning the byte size of
         *      samples.
         */
        uint16_t sampleSize;
    };
}; /* namespace Voceliand */

#endif // RECORD_AUDIO_INPUT_DEVICE_HPP
