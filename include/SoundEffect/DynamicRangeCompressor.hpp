/*
 *  File:   DynamicRangeCompressor.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 */

#ifndef VOCELIAND_DYNAMIC_RANGE_COMPRESSOR_HPP
#define VOCELIAND_DYNAMIC_RANGE_COMPRESSOR_HPP

/* standard library includes */
#include <vector>
#include <cmath>

/* external library includes */
/* project includes */
#include "SoundEffect.hpp"
#include "../Time.hpp"

namespace Voceliand
{
    #define Voceliand_min(a, b) ((a) < (b) ? (a) : (b))
    #define Voceliand_max(a, b) ((a) > (b) ? (a) : (b))

    /**
     *  @class DynamicRangeCompressor
     *      Perform audio compression over the signal given by the input
     *      vector of samples and writes the resulting signal into output.
     *
     *  @property {std::vector<char> const *} input
     *      Input vector of samples, not modified
     *  @property {std::vector<char> *} output
     *      Output vector of samples, result of the sound function.
     *
     *  @note
     *      The function process data "in-place", meaning you can use the
     *      same input and output vector if you want to overwrite the signal
     *      without fear.
     */
    class DynamicRangeCompressor : public SoundEffect
    {
    public:
        /**
         *  @function DynamicRangeCompressor
         *      Sets all parameters and values to default and input and output
         *      to null. Any parameter X is to be given inside the setX()
         *      functions.
         */
        DynamicRangeCompressor(void)
        {
            this->input = NULL;
            this->output = NULL;
            resetValues();
            resetParameters();
        }

        /**
         *  @function resetValues
         *      Reset internal values used by the compressor.
         *      Can be done manually, or be called by input or output setters.
         */
        void resetValues(void)
        {
            /* Clarity */
            uint32_t const channelCount = AudioConfiguration::sampleInfo.channelCount;

            this->previousLowPassLevel.resize(channelCount);
            this->previousSmoothedGainDB.resize(channelCount);

            for (size_t i = 0; i < channelCount; i++)
            {
                this->previousLowPassLevel[i] = 1.0;
                this->previousSmoothedGainDB[i] = 0.0;
            }
        }

        /**
         *  @function resetParameters
         *      Reset the compressor's parameters to default values.
         */
        void resetParameters(void)
        {
            /* default to no pre-gain */
            setPreGain(0.0);
            /* default threshold to -20.0 dBFS */
            setThreshold(-20.0);
            /* default ratio to 5 */
            setRatio(5.0);
            /* default attack time to 5 milliseconds */
            setAttack(Time::inMilliseconds(5));
            /* default release time to 200 milliseconds */
            setRelease(Time::inMilliseconds(200));
        }

        /* --- GETTERS AND SETTERS --- */

        void setInput(std::vector<char> const * input)
        {
            this->input = input;
            this->resetValues();
        }
        void setOutput(std::vector<char> * output)
        {
            this->output = output;
            this->resetValues();
        }
        void setPreGain(double preGain) {this->preGain = preGain;}
        /**
         *  @function setPreGainDB
         *      Like .setPreGain(), but set the pre-gain from a dB value.
         */
        void setPreGainDB(double preGainDB)
        {
            setPreGain(std::pow(10.0, preGainDB / 20.0));
        }
        void setRatio(double ratio)
        {
            double const MINIMUM_RATIO = 1e-3;
            /* Limit to positive values. */
            this->ratio = Voceliand_max(MINIMUM_RATIO, ratio);
        }
        void setThreshold(double threshold) {this->threshold = threshold;}
        void setAttack(Time attack)
        {
            this->attack = attack;

            /* Factor computation. */
            double seconds = this->attack.asSeconds();
            uint32_t sampleRate = AudioConfiguration::sampleInfo.rate;
            /* Safety check. */
            if(sampleRate != 0)
            {
                this->attackFactor = std::exp(-1.0 / (seconds * sampleRate));
            }

            /* Also update the time constant for the envelope extraction.
             * Make the level time-constant one-fifth the gain time constants.
             */
            setSmoothingTime(Voceliand_min(this->attack, this->release) / 5.0f);
        }
        void setRelease(Time release)
        {
            this->release = release;

            /* Factor computation. */
            double seconds = this->release.asSeconds();
            uint32_t sampleRate = AudioConfiguration::sampleInfo.rate;
            /* Safety check. */
            if(sampleRate != 0)
            {
                this->releaseFactor = std::exp(-1.0 / (seconds * sampleRate));
            }

            /* Also update the time constant for the envelope extraction.
             * Make the level time-constant one-fifth the gain time constants.
             */
            setSmoothingTime(Voceliand_min(this->attack, this->release) / 5.0f);
        }
        /**
         *  @function setSmoothingTime
         *      -MEH-?
         */
        void setSmoothingTime(Time smoothingTime)
        {
            /* This is the minimum allowed value. */
            static Time const MINIMUM_SMOOTHING_TIME = Time::inMilliseconds(2);
            this->smoothingTime = Voceliand_max(MINIMUM_SMOOTHING_TIME,
                smoothingTime);

            /* Update the associated factor. */
            double seconds = this->smoothingTime.asSeconds();
            uint32_t sampleRate = AudioConfiguration::sampleInfo.rate;
            /* Safety check. */
            if(sampleRate != 0)
            {
                this->smoothingFactor = std::exp(-1.0 / (seconds * sampleRate));
            }
        }

        double getPreGain(void) {return this->preGain;}
        /**
         *  @function getPreGainDB
         *      Like getPreGain, but the result here is in dB.
         */
        double getPreGainDB(void) {return 20.0 * std::log10(this->preGain);}
        double getRatio(void) {return this->ratio;}
        double getThreshold(void) {return this->threshold;}
        Time getAttack(void) {return this->attack;}
        Time getRelease(void) {return this->release;}
        Time getSmoothingTime(void) {return this->smoothingTime;}
        /**
         *  @function getCurrentLeveldBFS
         *      For visualization, returns the current dB level of the signal.
         *      -MEH-
         *      Average value of channels.
         */
        double getCurrentLeveldBFS(void)
        {
            double sum = 0.0;
            uint32_t const channelCount = AudioConfiguration::sampleInfo.channelCount;

            for (size_t channel = 0; channel < channelCount; channel++)
            {
                sum = 10.0 * std::log10(this->previousLowPassLevel[channel]);
            }

            return sum / channelCount;
        }
        /**
         *  @function getCurrentGaindBFS
         *      For visualization, returns the gain in dB that is currently
         *      applied to the signal.
         **     Average value of channels.
         */
        double getCurrentGaindBFS(void)
        {
            double sum = 0.0;
            uint32_t const channelCount = AudioConfiguration::sampleInfo.channelCount;

            for (size_t channel = 0; channel < channelCount; channel++)
            {
                sum = 10.0 * std::log10(this->previousSmoothedGainDB[channel]);
            }

            return sum / channelCount;
        }

        /**
         *  @function process
         *      Performs the compression over the signal (given by .input
         *      and .output pointers).
         *      Do nothing if any end of the signal is not set.
         */
        void process(void);

    private:
        /**
         *  @function computeDBLevel
         *      Extract the dB level of the signal from its amplitude.
         *
         *  @param {std::vector<double> const &} input
         *      The input vector of sample, as double precision values.
         *  @param {std::vector<double> &} dBLevel
         *      The computed dB level.
         */
        void computeDBLevel(std::vector<double> const & input,
            std::vector<double> & dBLevel);
        void computeInstantaneousTargetGain(std::vector<double> const & dBLevel,
            std::vector<double> & instantaneousGain);
        void computeSmoothedGain(std::vector<double> const & instantaneousGain,
            std::vector<double> & smoothedGain);
        void computeDynamicGain(std::vector<double> const & dBLevel,
            std::vector<double> & gain);

        /**
         *  @property {std::vector<char> const *} input
         *      A pointer to the input signal vector. Must be set before calling
         *      .process().
         */
        std::vector<char> const * input;
        /**
         *  @property {std::vector<char> *} output
         *      A pointer to the output signal vector. Must be set before
         *      calling .process().
         */
        std::vector<char> * output;

        /**
         *  @property {double} preGain
         *      Eventual pre-gain to apply to the signal
         */
        double preGain;
        /**
         *  @property {double} ratio
         *      Compression ratio.
         *      Correspond to input[i] - threshold) / (output[i] - threshold)
         *      in the gain equation.
         */
        double ratio;
        /**
         *  @property {double} threshold
         *      Threshold (in dBFS) at which the compression triggers.
         */
        double threshold;
        /**
         *  @property {Time} attack
         *      Time before correcting the signal after going above threshold.
         */
        Time attack;
        /**
         *  @property {Time} release
         *      Time before correcting the signal before going under threshold.
         */
        Time release;
        /**
         *  @property {double} smoothingTime
         *      For the amplitude-to-dB low-pass filtering. Time interval for
         *      the differential equation.
         *      Currently computed from attack and release values, but -MEH-?
         */
        Time smoothingTime;
        /**
         *  @property {double} attackFactor
         *      Computed from attack. Is used to smooth the gain accordingly.
         */
        double attackFactor;
        /**
         *  @property {double} releaseFactor
         *      Computed from release. Is used to smooth the gain accordingly.
         */
        double releaseFactor;
        /**
         *  @property {double} smoothingFactor
         *      Computed from smoothing time.
         *      Used in the amplitude-to-dB low-pass filtering accordingly.
         */
        double smoothingFactor;
        /**
         *  @property {std::vector<double>} previousLowPassLevel
         *      Saves the last value of the low-pass filter used in the
         *      amplitude-to-dB computation, since it is necessary for the
         *      equation.
         *      Each vector element is for the corresponding channel.
         */
        std::vector<double> previousLowPassLevel;
        /**
         *  @property {std::vector<double>} previousSmoothedGainDB
         *      Saves the last value of the smoothed gain in the peak detector,
         *      since it is required for the equation.
         *      Each vector element is for the corresponding channel.
         */
        std::vector<double> previousSmoothedGainDB;
    }; /* class DynamicRangeCompressor */

    #undef Voceliand_min
    #undef Voceliand_max
}; /* namespace Voceliand */

#endif // VOCELIAND_DYNAMIC_RANGE_COMPRESSOR_HPP
