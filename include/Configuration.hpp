/*
 *  File:   Configuration.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

#ifndef VOCELIAND_CONFIGURATION_HPP
#define VOCELIAND_CONFIGURATION_HPP

/* standard library include */
/* external library include */
/* project include */

/* Identify the operating system */
/* see http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system */
#if defined(_WIN32)
    /* Windows */
    #define VOCELIAND_SYSTEM_WINDOWS
#elif defined(__APPLE__) && defined(__MACH__)
    /* Apple platform, see which one it is */
    #include "TargetConditionals.h"

    #if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
        /* iOS */
        #define VOCELIAND_SYSTEM_IOS
    #elif TARGET_OS_MAC
        /* MacOS */
        #define VOCELIAND_SYSTEM_MACOS
    #else
        /* Unsupported Apple system */
        #error Unsupported Apple system
    #endif
#elif defined(__unix__)
    /* UNIX system, see which one it is */
    #if defined(__ANDROID__)
        /* Android */
        #define VOCELIAND_SYSTEM_ANDROID
    #elif defined(__linux__)
         /* Linux */
        #define VOCELIAND_SYSTEM_LINUX
    #elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
        /* FreeBSD */
        #define VOCELIAND_SYSTEM_FREEBSD
    #else
        /* Unsupported UNIX system */
        #error Unsupported UNIX system
    #endif
#else
    /* Unsupported system */
    #error Unsupported system
#endif

#endif // VOCELIAND_CONFIGURATION_HPP
