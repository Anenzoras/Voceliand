/*
 *  File:   UI.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-29
 */

#ifndef VOCELIAND_UI_HPP
#define VOCELIAND_UI_HPP

/* standard library include */

/* external library include */
#include <GLFW/glfw3.h>     // cross-platform windows opener

/* project include */
#include "../Status.hpp"

namespace Voceliand
{
    namespace UI
    {
        // Data
        extern GLFWwindow * window;

        /**
         *  @function initialize
         *      Library entry point, completelly initialize glfw/glew/soundio/dear
         *      imgui.
         *
         *  @returns {Status::Code}
         *      Status::INITIALIZATION_GLFW_FAILURE if glfw couldn't initialize
         *      properly.
         *      Status::INITIALIZATION_GLEW_FAILURE if glew couldn't initialize
         *      properly.
         *      Status::SUCESS if all went well.
         */
        Status::Code initialize(void);

        /**
         *  @function shutdown
         *      Deactivate user interface by shuting down devices, dear imgui
         *      and glfw.
         */
        void shutdown(void);

        /**
         *  @function newFrame
         *      Routine to prepare the new frame drawing.
         */
        void newFrame(void);
    };
};

#endif // VOCELIAND_UI_HPP
