##### Variables

# Compiler and linker (default) and flags
BASECXX := g++
CFLAGS := -g -Wall -Wextra -Werror
LDFLAGS :=

# Helpers
MD ?= mkdir
MDFLAGS := -pv

RM ?= rm
RMFLAGS := -r

# Paths
SRC := source
OBJ := object
LIB := libraries
SFX := cpp

NAME := Voceliand

# If unset, default target is win32
TARGET ?= win32

# Inspired from here http://stackoverflow.com/a/12959694
# Way to seek everything - used for automatic subdirectory & file name finding
getFullFileTree = $(wildcard $1/*) $(foreach truc, $(wildcard $1/*), $(call getFullFileTree, $(truc)))

SRC_TREE := $(call getFullFileTree, $(SRC))

SRC_DIRS := $(sort $(dir $(SRC_TREE)))
SRCS := $(filter %.$(SFX), $(SRC_TREE))

OBJ_DIRS := $(patsubst $(SRC)/%, $(OBJ)/%, $(SRC_DIRS))
OBJS := $(patsubst $(SRC)/%.$(SFX), $(OBJ)/%.o, $(SRCS))

# Target related variables and extras
RELEASE := binaries/$(TARGET)

# "Dynamic" variables
ifeq ($(TARGET), win32)
	CXX := i686-w64-mingw32-$(BASECXX)
	CPLATFORM := -m32
	LD := i686-w64-mingw32-$(BASECXX)
	LDPLATFORM := -m32
	LDSPECIFIC := -lwinmm
	EXEC := $(RELEASE)/$(NAME).exe
	LOPENGL := opengl32
else ifeq ($(TARGET), win64)
	CXX := x86_64-w64-mingw32-$(BASECXX)
	CPLATFORM := -m64
	LD := x86_64-w64-mingw32-$(BASECXX)
	LDPLATFORM := -m64
	LDSPECIFIC := -lwinmm
	EXEC := $(RELEASE)/$(NAME).exe
	LOPENGL := opengl32
else ifeq ($(TARGET), linux32)
	$(error Not supported yet, on the todo list)
	CXX := $(BASECXX)
	CPLATFORM := -m32
	LD := $(BASECXX)
	LDPLATFORM := -m32
	LDSPECIFIC := -lwinmm
	EXEC := $(RELEASE)/$(NAME)
	LOPENGL := GL
else ifeq ($(TARGET), linux64)
	$(error Not supported yet, on the todo list)
	CXX := $(BASECXX)
	CPLATFORM := -m64
	LD := $(BASECXX)
	LDPLATFORM := -m64
	LDSPECIFIC := -lwinmm
	EXEC := $(RELEASE)/$(NAME)
	LOPENGL := GL
else ifeq ($(TARGET), osx)
	$(error Not supported yet, not on the todo list -but hey, cross platform is cross platform I guess-)
	CXX := $(BASECXX)
	CPLATFORM := -m64
	LD := $(BASECXX)
	LDPLATFORM := -m64
	LDSPECIFIC := -lwinmm
	EXEC := $(RELEASE)/$(NAME)
	LOPENGL := GL
else
	# Crash if target is bullshit
	$(error TARGET should be linux32/64 or win32/64 -or osx-)
endif

# LIBJPEG = $(LIB)/$(TARGET)/jpeg-8
LIBGLFW := $(LIB)/glfw-3.2.1
LIBGLEW := $(LIB)/glew-2.0.0
LIBIMGUI := $(LIB)/imgui
LIBSOUNDIO := $(LIB)/libsoundio-1.1.0
CEXTRA := -I$(LIBGLFW)/include -I$(LIBGLEW)/include -I$(LIBIMGUI)/include -I$(LIBSOUNDIO)/include
LDEXTRA := -L$(LIBGLFW)/lib/$(TARGET) -lglfw3 -L$(LIBGLEW)/lib/$(TARGET) -lglew32 -L$(LIBIMGUI)/lib/$(TARGET) -limgui -L$(LIBSOUNDIO)/lib/$(TARGET) -llibsoundio -l$(LOPENGL) $(LDSPECIFIC)

##### Rules

.PHONY: all build clean folders run

all: folders build

build: $(EXEC)

clean:
	$(RM) $(RMFLAGS) $(OBJ)/

# incompatible with run
clear:
	$(RM) $(RMFLAGS) $(EXEC) $(OBJ)/

folders:
	$(MD) $(MDFLAGS) $(OBJ_DIRS)

run:
	export LD_LIBRARY_PATH=$(RELEASE) && $(EXEC)

###

$(EXEC): $(OBJS)
	$(LD) $(LDPLATFORM) $(LDFLAGS) $^ -o $@ $(LDEXTRA)

$(OBJ)/%.o: $(SRC)/%.$(SFX)
	$(CXX) $(CPLATFORM) $(CFLAGS) -c $< -o $@ $(CEXTRA)
