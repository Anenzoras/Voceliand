/*
 *  File:   DriverLoader.cpp
 *  Author: Anenzoras
 *  Date:   2019-01-20
 *  Brief:  Contains a small main to detect and list devices in windows, for
 *  testing purposes.
 */

#include <mmdeviceapi.h>
#include <iostream>

int main(int argc, char const *argv[])
{
    (void) argc;
    (void) argv;

    CLSID const CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
    IID const IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);

    HRESULT hr = NOERROR;
    IMMDeviceEnumerator * pEnumerator = NULL;
    IMMDeviceCollection * pCollection = NULL;
    IMMDevice * pDevice = NULL;
    unsigned int count = 0;

    std::cout << "Press enter to continue" << std::endl;
    (void) getc(stdin);

    hr = CoInitialize(NULL);
    if (hr != S_OK)
    {
        std::cerr << "CoInitialize failed" << std::endl;
        return 0;
    }

    hr = CoCreateInstance(CLSID_MMDeviceEnumerator,
        NULL,
        CLSCTX_ALL,
        IID_IMMDeviceEnumerator,
        (void**) &pEnumerator);
    if (hr != S_OK)
    {
        std::cerr << "CoCreateInstance failed" << std::endl;
        return 0;
    }

    hr = pEnumerator->EnumAudioEndpoints(eAll,
        DEVICE_STATEMASK_ALL,
        &pCollection);
    if (hr != S_OK)
    {
        std::cerr << "EnumAudioEndpoints failed" << std::endl;
        pEnumerator->Release();
        return 0;
    }

    hr = pCollection->GetCount(&count);
    if (hr != S_OK)
    {
        std::cerr << "GetCount failed" << std::endl;
        pCollection->Release();
        pEnumerator->Release();
        return 0;
    }

    std::cout << "Count: " << count << std::endl;

    for (unsigned int i = 0; i < count; i++)
    {
        wchar_t * id = NULL;

        std::cout << "Item: " << i << std::endl;

        hr = pCollection->Item(i, &pDevice);
        if (hr != S_OK)
        {
            std::cerr << "Item failed" << std::endl;
        }
        else
        {
            hr = pDevice->GetId(&id);
            if (hr != S_OK)
            {
                std::cerr << "GetId failed" << std::endl;
            }
            else
            {
                std::wcout << "    " << id << std::endl;
            }

            CoTaskMemFree(id);
        }

        if (pDevice) pDevice->Release();
    }

    if (pCollection) pCollection->Release();
    if (pEnumerator) pEnumerator->Release();

    CoFreeUnusedLibraries();
    CoUninitialize();

    return 0;
}

/*
x86_64-w64-mingw32-g++ -m64 -O2 -Wall -Wextra -Werror DetectDevice.cpp -o DetectDevice.exe
*/
