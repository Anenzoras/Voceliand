/*
 *  File:   DllHookTest.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-04
 *
 *  Brief:  A test to hook core audio API using the proxy COM object method
 *      (the other method being the vtable patching, maybe cleaner but less
 *      easy).
 */


/* Standard library includes */
#include <cstdio>

/* External library includes */
#include <Windows.h>  	/* GetModuleFileNameA() */
#include <tlhelp32.h> 	/* Toolhelp to get running processes */
#include <psapi.h>		/* EnumProcessModules(), GetModuleFileNameExA() */

/* Project includes */

/* --- */

/**
 *  @function findProcess
 *      Walks through snapshot of processes in memory, compares with given
 *      process.
 *
 *      Taken from: https://github.com/Arvanaghi/Windows-DLL-Injector/blob/master/Source/DLL_Injector.c
 *
 *  @param {WCHAR const *} processName
 *      Name of the process to find in memory.
 *
 *  @returns {HANDLE}
 *      HANDLE object of the process if found
 *      NULL otherwise
 */
HANDLE findProcess(WCHAR const * processName)
{
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
    {
		printf("[---] Could not create snapshot.\n");
        return NULL;
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
    {
        printf("[---] Could not access first process of the list.\n");
		CloseHandle(hProcessSnap);
		return NULL;
	}

	// Now walk the snapshot of processes, and
	// display information about each process in turn
	do
    {
        WCHAR const * wszExeFile;

        /* Change char * to wchar * */
        int nChars = MultiByteToWideChar(CP_ACP, 0, pe32.szExeFile, -1, NULL, 0);
        wszExeFile = new WCHAR[nChars];
        MultiByteToWideChar(CP_ACP, 0, pe32.szExeFile, -1, (LPWSTR)wszExeFile,
            nChars);

		if (!wcscmp(wszExeFile, processName))
        {
			wprintf(L"[+] The process %ls was found in memory.\n", wszExeFile);

			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
			if (hProcess != NULL)
            {
				return hProcess;
			}
            else
            {
				printf("[---] Failed to open process %s.\n", pe32.szExeFile);
				return NULL;
			}
		}

        /* Cleanup */
        delete[] wszExeFile;
	} while (Process32Next(hProcessSnap, &pe32));

	wprintf(L"[---] %ls has not been loaded into memory, aborting.\n", processName);

	return NULL;
}

/**
 *  @function injectDLL
 *      Inject dll from given dllPath into given process.
 *      Gets LoadLibraryA address from current process, which is guaranteed to
 *      be same for single boot session across processes.
 *
 *      Taken from: https://github.com/Arvanaghi/Windows-DLL-Injector/blob/master/Source/DLL_Injector.c
 *
 *  @param {HANDLE} hProcess
 *      Process handle object, so that windows can do shit with.
 *  @param {char const *} dllPath
 *      The path to the DLL to load.
 *
 *  @returns {bool}
 *      TRUE if creating a thread in remote process calling LoadLibraryA is
 *			successful. This does not imply that injection is successful,
 *			because we have no way of knowing that.
 *      FALSE if we couldn't create a remote thread.
 */
bool injectDLL(HANDLE hProcess, char const * dllPath)
{
	// Allocate memory for DLL's path name to remote process
	LPVOID dllPathAddressInRemoteMemory = NULL;
    BOOL succeededWriting = FALSE;
    LPVOID loadLibraryAddress = NULL;

    dllPathAddressInRemoteMemory = VirtualAllocEx(hProcess,
        NULL,
        strlen(dllPath),
        MEM_RESERVE | MEM_COMMIT,
        PAGE_EXECUTE_READWRITE);
	if (dllPathAddressInRemoteMemory == NULL)
    {
		printf("[---] VirtualAllocEx unsuccessful.\n");
		return FALSE;
	}

	// Write DLL's path name to remote process
	succeededWriting = WriteProcessMemory(hProcess,
        dllPathAddressInRemoteMemory,
        dllPath,
        strlen(dllPath),
        NULL);
	if (!succeededWriting)
    {
		printf("[---] WriteProcessMemory unsuccessful.\n");
		return FALSE;
	}

	// Returns a pointer to the LoadLibrary address.
    // This will be the same on the remote process as in our current process.
	loadLibraryAddress = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"),
        "LoadLibraryA");
	if (loadLibraryAddress == NULL)
    {
		printf("[---] LoadLibrary not found in process.\n");
		return FALSE;
	}

	HANDLE remoteThread = CreateRemoteThread(hProcess,
        NULL,
        NULL,
        (LPTHREAD_START_ROUTINE)loadLibraryAddress,
        dllPathAddressInRemoteMemory,
        NULL,
        NULL);
	if (remoteThread == NULL)
    {
		printf("[---] CreateRemoteThread unsuccessful.\n");
		return FALSE;
	}

	WaitForSingleObject(remoteThread, INFINITE);
	VirtualFreeEx(hProcess, dllPathAddressInRemoteMemory, strlen(dllPath), MEM_RELEASE);
	CloseHandle(remoteThread);

	return TRUE;
}

/**
 *  @function ejectDLL
 *      Eject the dll of given dllPath from given process.
 *      Gets FreeLibrary address from current process, which is guaranteed to
 *      be same for single boot session across processes.
 *
 *  @param {HANDLE} hProcess
 *      Process handle object, so that windows can do shit with.
 *  @param {char const *} dllPath
 *      The path to the DLL to withdraw.
 *
 *  @returns {bool}
 *      TRUE if creating a thread in remote process calling FreeLibrary is
 *			successful. This does not imply that withdrawal is successful,
 *			because we have no way of knowing that.
 *      FALSE if we couldn't create a remote thread.
 */
bool ejectDLL(HANDLE hProcess, char const * dllPath)
{
	// Allocate memory for DLL's path name to remote process
	BOOL success = FALSE;
	HMODULE hModuleDLL;

	/* Seek module in remote process */
	{
		DWORD arraySize = 0;
		DWORD neededSize = 64; /* Arbitrary 64 bytes */
		size_t hModuleCount = 0;
		HMODULE * hModules = NULL;
		HMODULE * tmp = NULL;
		char modulePath[MAX_PATH];
		DWORD fileNameSize;
		int result;
		bool found = FALSE;

		while (arraySize < neededSize)
		{
			arraySize = neededSize;
			tmp = (HMODULE *) realloc(hModules, neededSize);
			if (tmp == NULL)
			{
				if (hModules)
				{
					delete hModules;
				}

				printf("realloc() out of memory\n");

				return FALSE;
			}

			hModules = tmp;

			success = EnumProcessModules(hProcess, hModules, arraySize,
				&neededSize);
			if (!success)
			{
				printf("[---] EnumProcessModules() failed.\n");

				delete hModules;

				return FALSE;
			}
		}

		/* Here hModules contains all the process's hmodules, arraySize the
		 * array's memory size and neededSize the returned size (and thus gives
		 * us the number of modules) */

		hModuleCount = neededSize / sizeof(HMODULE);

		for (size_t i = 0; i < hModuleCount; i++)
		{
			fileNameSize = GetModuleFileNameExA(hProcess, hModules[i],
				modulePath, MAX_PATH);
			if (fileNameSize == 0)
			{
				printf("[---] GetModuleFileNameExA() failed.\n");

				delete hModules;

				return FALSE;
			}

			result = strcmp(modulePath, dllPath);
			if (result == 0)
			{
				found = TRUE;
				hModuleDLL = hModules[i];

				break;
			}
		}

		delete hModules;

		if (!found)
		{
			printf("[---] The DLL could not be found in loaded modules");

			return FALSE;
		}
	}

	/* Actually call FreeLibrary in remote process */
	{
		LPVOID freeLibraryAddress = NULL;

		// Returns a pointer to the FreeLibrary address.
		// This will be the same on the remote process as in our current process.
		freeLibraryAddress = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"),
			"FreeLibrary");
		if (freeLibraryAddress == NULL)
		{
			printf("[---] FreeLibrary not found in process.\n");
			return FALSE;
		}

		HANDLE remoteThread = CreateRemoteThread(hProcess,
			NULL,
			NULL,
			(LPTHREAD_START_ROUTINE)freeLibraryAddress,
			hModuleDLL,
			NULL,
			NULL);
		if (remoteThread == NULL)
		{
			printf("[---] CreateRemoteThread unsuccessful.\n");
			return FALSE;
		}

		WaitForSingleObject(remoteThread, INFINITE);

		// Get thread exit code
		DWORD exitCode;
		if (!GetExitCodeThread(remoteThread, &exitCode))
		{
			printf("[---] Could not get thread exit code.\n");
		}

		if (!exitCode)
		{
			printf("[---] FreeLibrary failed.\n");
		}

		CloseHandle(remoteThread);
	}

	return TRUE;
}

int main(int argc, char const * argv[])
{
    HANDLE hProcess;
    bool success = TRUE;
    int c = 0;
	char dirPath[MAX_PATH];
	char dllPath[MAX_PATH];
	bool arch32 = TRUE; // Is program running in 32 or 64 bits

	if (argc > 2)
	{
		fprintf(stdout, "Invalid number of argument\n"
			"Usage: %s [32|64] (default: 32)\n\n", argv[0]);
		fflush(stdout);

		return 0;
	}

	if (argc == 2)
	{
		if (strcmp(argv[1], "32") == 0)
		{
			arch32 = TRUE;
		}
		else if (strcmp(argv[1], "64") == 0)
		{
			arch32 = FALSE;
		}
		else
		{
			fprintf(stdout, "Invalid argument\n"
				"Usage: %s [32|64] (default: 32)\n\n", argv[0]);
		}
	}

	/* Get path */
	{
		char const SEPARATOR = '\\';
		DWORD stringSize = 0;
		size_t i;

		stringSize = GetModuleFileNameA(NULL, dirPath, MAX_PATH);
		if (stringSize == 0)
		{
			fprintf(stderr, "Couln't retrieve the DLL's pathes.\n");
			fflush(stderr);

			return -1;
		}

		/* Truncate path to remove exec name */
		i = stringSize - 1;
		while ((i > 0) && (dirPath[i] != SEPARATOR))
		{
			i--;
		}

		if (dirPath[i] != SEPARATOR)
		{
			fprintf(stderr, "Couln't retrieve the DLLs' platform (1).\n");
			fflush(stderr);

			return -1;
		}

		stringSize = i;
		dirPath[i] = '\0';

		if (arch32)
		{
			strncat(dirPath, "\\binaries\\win32", MAX_PATH);
		}
		else
		{
			// 64bit
			strncat(dirPath, "\\binaries\\win64", MAX_PATH);
		}
	}

    // hProcess = findProcess(L"DetectDevice.exe");
    hProcess = findProcess(L"audacity.exe");
    if (hProcess == NULL)
	{
		return 1;
	}

	if (arch32)
	{
		strcpy(dllPath, dirPath);
		strncat(dllPath, "\\libgcc_s_sjlj-1.dll", MAX_PATH);
	}
	else
	{
		// 64 bit
		strcpy(dllPath, dirPath);
		strncat(dllPath, "\\libgcc_s_seh-1.dll", MAX_PATH);
	}
	success = injectDLL(hProcess, dllPath);
	strcpy(dllPath, dirPath);
	strncat(dllPath, "\\libwinpthread-1.dll", MAX_PATH);
	success = success && injectDLL(hProcess, dllPath);
	strcpy(dllPath, dirPath);
	strncat(dllPath, "\\libstdc++-6.dll", MAX_PATH);
	success = success && injectDLL(hProcess, dllPath);
	if (arch32)
	{
		strcpy(dllPath, dirPath);
		strncat(dllPath, "\\MinHook.x86.dll", MAX_PATH);
	}
	else
	{
		// 64 bit
		strcpy(dllPath, dirPath);
		strncat(dllPath, "\\MinHook.x64.dll", MAX_PATH);
	}
	success = success && injectDLL(hProcess, dllPath);
	strcpy(dllPath, dirPath);
	strncat(dllPath, "\\DllHook.dll", MAX_PATH);
	success = success && injectDLL(hProcess, dllPath);
    if (!success)
    {
        fprintf(stdout, "Impossible to hook.\n");
        fflush(stdout);

        return 1;
    }

    fprintf(stdout, "Press enter to quit.\n");
    fflush(stdout);

    c = getc(stdin);
    (void) c;

	/* Cleanup */
	success = ejectDLL(hProcess, dllPath); // It's MinHook

	#undef BASE_PATH
	#undef PLATFORM_DIR
	#undef FULL_PATH

	CloseHandle(hProcess);

    return 0;
}
