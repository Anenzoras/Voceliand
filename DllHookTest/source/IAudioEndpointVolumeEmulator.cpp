/*
 *  File:   IAudioEndpointVolumeEmulator.cpp
 *  Author: Anenzoras
 *  Date:   2019-03-15
 */

/* Standard library includes */
#include <cmath>

/* Project includes */
#include "IAudioEndpointVolumeEmulator.hpp"

/**
 *  @property {const IID} IID_IAudioEndpointVolume
 *      Our identifier.
 */
const IID IID_IAudioEndpointVolume = __uuidof(IAudioEndpointVolume);

/*
 * IAudioEndpointVolumeEmulator stuff
 */
/* Static function to retrieve an instance of the IAudioEndpointVolumeEmulator
 * classes, to go along MS syntax. */
IAudioEndpointVolumeEmulator * IAudioEndpointVolumeEmulator::CreateInstance(
    IMMDeviceEmulator * device)
{
    if (device == NULL)
    {
        return NULL;
    }

    IAudioEndpointVolumeEmulator * emulator =
        new IAudioEndpointVolumeEmulator(device);
    if (emulator == NULL)
    {
        return NULL;
    }

    emulator->AddRef();

    return emulator;
}

/* Constructor. Private for reasons. Stores the given pointer. */
IAudioEndpointVolumeEmulator::IAudioEndpointVolumeEmulator(
    IMMDeviceEmulator * device)
{
    this->referenceCount = 0;
    this->device = device;
}

/* Destructor. */
IAudioEndpointVolumeEmulator::~IAudioEndpointVolumeEmulator()
{
    this->device = NULL;
}

/*
 * IUnknown stuff
 */
/* Increments the reference count for an interface on an object. This method
 * should be called for every new copy of a pointer to an interface on an
 * object. */
ULONG IAudioEndpointVolumeEmulator::AddRef()
{
    InterlockedIncrement(&this->referenceCount);

    return this->referenceCount;
}

/* Decrements the reference count for an interface on an object. */
ULONG IAudioEndpointVolumeEmulator::Release()
{
    ULONG ref = InterlockedDecrement(&this->referenceCount);
    if (this->referenceCount == 0)
    {
        /* End of life */
        delete this;
    }

    return ref;
}

/* Retrieves pointers to the supported interfaces on an object. */
HRESULT IAudioEndpointVolumeEmulator::QueryInterface(REFIID riid,
    void ** ppvObject)
{
    if (ppvObject == NULL)
    {
        return E_POINTER;
    }

    (*ppvObject) = NULL;

    HRESULT result = E_NOINTERFACE;

    if (IsEqualIID(riid, IID_IUnknown))
    {
        (*ppvObject) = this;

        result = S_OK;
    }
    else if (IsEqualIID(riid, IID_IAudioEndpointVolume))
    {
        (*ppvObject) = static_cast<IAudioEndpointVolume *>(this);

        result = S_OK;
    }

    if (result == S_OK)
    {
        this->AddRef();
    }

    return result;
}

/*
 * IAudioEndpointVolume stuff
 */

/* The GetChannelCount method gets a count of the channels in the audio stream
 * that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetChannelCount(UINT * pnChannelCount)
{
    if (pnChannelCount == NULL)
    {
        return E_POINTER;
    }

    (*pnChannelCount) = this->device->sessionInfo.channelCount;

    return S_OK;
}


/* The GetChannelVolumeLevel method gets the volume level, in decibels, of the
 * specified channel in the audio stream that enters or leaves the audio
 * endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetChannelVolumeLevel(UINT nChannel,
   float * pfLevelDB)
{
    if (nChannel >= this->device->sessionInfo.channelCount)
    {
        return E_INVALIDARG;
    }

    if (pfLevelDB == NULL)
    {
        return E_POINTER;
    }

    float baseValue = this->device->sessionInfo.volumes[nChannel];

    if (baseValue < 0.01f)
    {
        /* log otherwise, so avoid undefined value 0 */
        (*pfLevelDB) = -120.0f;
    }
    else
    {
        (*pfLevelDB) = this->device->sessionInfo.dBFactor
            * std::log10(baseValue);
    }

    return S_OK;
}

/* The GetChannelVolumeLevelScalar method gets the normalized, audio-tapered
 * volume level of the specified channel of the audio stream that enters or
 * leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetChannelVolumeLevelScalar(UINT nChannel,
    float * pfLevel)
{
    if (nChannel >= this->device->sessionInfo.channelCount)
    {
        return E_INVALIDARG;
    }

    if (pfLevel == NULL)
    {
        return E_POINTER;
    }

    (*pfLevel) = this->device->sessionInfo.volumes[nChannel];

    return S_OK;
}

/* The GetMasterVolumeLevel method gets the master volume level, in decibels, of
 * the audio stream that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetMasterVolumeLevel(float * pfLevelDB)
{
    if (pfLevelDB == NULL)
    {
        return E_POINTER;
    }

    float baseValue = this->device->sessionInfo.masterVolume;

    if (baseValue < 0.01f)
    {
        /* log otherwise, so avoid undefined value 0 */
        (*pfLevelDB) = -120.0f;
    }
    else
    {
        (*pfLevelDB) = this->device->sessionInfo.dBFactor
            * std::log10(baseValue);
    }

    return S_OK;
}

/* The GetMasterVolumeLevelScalar method gets the master volume level of the
 * audio stream that enters or leaves the audio endpoint device. The volume
 * level is expressed as a normalized, audio-tapered value in the range from
 * 0.0 to 1.0. */
HRESULT IAudioEndpointVolumeEmulator::GetMasterVolumeLevelScalar(
    float * pfLevel)
{
    if (pfLevel == NULL)
    {
        return E_POINTER;
    }

    (*pfLevel) = this->device->sessionInfo.masterVolume;

    return S_OK;
}

/* The GetMute method gets the muting state of the audio stream that enters or
 * leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetMute(BOOL * pbMute)
{
    if (pbMute != NULL)
    {
        return E_POINTER;
    }

    (*pbMute) = this->device->sessionInfo.muted;

    return S_OK;
}

/* The GetVolumeRange method gets the volume range, in decibels, of the audio
 * stream that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::GetVolumeRange(float * pflVolumeMindB,
    float * pflVolumeMaxdB, float * pflVolumeIncrementdB)
{
    if ((pflVolumeMindB == NULL)
        || (pflVolumeMaxdB == NULL)
        || (pflVolumeIncrementdB == NULL))
    {
        return E_POINTER;
    }

    (*pflVolumeMindB) = this->device->sessionInfo.minDB;
    (*pflVolumeMaxdB) = this->device->sessionInfo.maxDB;
    (*pflVolumeIncrementdB) = this->device->sessionInfo.volumeIncrementDB;

    return S_OK;
}

/* The GetVolumeStepInfo method gets information about the current step in the
 * volume range. */
HRESULT IAudioEndpointVolumeEmulator::GetVolumeStepInfo(UINT * pnStep,
    UINT * pnStepCount)
{
    if ((pnStep == NULL) && (pnStepCount == NULL))
    {
        return E_POINTER;
    }

    UINT stepCount = this->device->sessionInfo.volumeStepCount;

    if (pnStep != NULL)
    {
        HRESULT result = S_OK;
        float dBLevel = 0.0f;
        float minDB = this->device->sessionInfo.minDB;
        float stepValue = this->device->sessionInfo.volumeIncrementDB;
        UINT step = 0;

        result = this->GetMasterVolumeLevel(&dBLevel);
        if (result != S_OK)
        {
            return E_FAIL;
        }

        /* Sum up: dBLevel is between 2 steps, this formula starts at the
         * bottom and checks if level_(step) < dbLevel < level_(step+1),
         * in which case we can return step. */
        while ((minDB + (((float)step + 1.0f) * stepValue)) < dBLevel)
        {
            step++;
        }

        (*pnStep) = step;
    }

    if (pnStepCount != NULL)
    {
        (*pnStepCount) = stepCount;
    }

    return S_OK;
}

/* The QueryHardwareSupport method queries the audio endpoint device for its
 * hardware-supported functions. */
HRESULT IAudioEndpointVolumeEmulator::QueryHardwareSupport(
    DWORD * pdwHardwareSupportMask)
{
    if (pdwHardwareSupportMask == NULL)
    {
        return E_POINTER;
    }

    (*pdwHardwareSupportMask) = 0;

    return S_OK;
}

/* The RegisterControlChangeNotify method registers a client's notification
 * callback interface. */
HRESULT IAudioEndpointVolumeEmulator::RegisterControlChangeNotify(
    IAudioEndpointVolumeCallback * pNotify)
{
    if (pNotify == NULL)
    {
        return E_POINTER;
    }

    HRESULT result = this->device->sessionInfo.addListener(pNotify);
    if (result != S_OK)
    {
        return result;
    }

    pNotify->AddRef();

    return S_OK;
}

/* The SetChannelVolumeLevel method sets the volume level, in decibels, of the
 * specified channel of the audio stream that enters or leaves the audio
 * endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::SetChannelVolumeLevel(UINT nChannel,
    float fLevelDB, LPCGUID pguidEventContext)
{
    if (nChannel >= this->device->sessionInfo.channelCount)
    {
        return E_INVALIDARG;
    }

    if ((fLevelDB < this->device->sessionInfo.minDB)
        || (fLevelDB > this->device->sessionInfo.maxDB))
    {
        return E_INVALIDARG;
    }

    float levelScalar = std::pow(10, fLevelDB / this->device->sessionInfo.dBFactor);

    this->device->sessionInfo.volumes[nChannel] = levelScalar;

    HRESULT status;
    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}

/* The SetChannelVolumeLevelScalar method sets the normalized, audio-tapered
 * volume level of the specified channel in the audio stream that enters or
 * leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::SetChannelVolumeLevelScalar(UINT nChannel,
    float fLevel, LPCGUID pguidEventContext)
{
    if (nChannel >= this->device->sessionInfo.channelCount)
    {
        return E_INVALIDARG;
    }

    if ((fLevel < 0.0f) || (fLevel > 1.0f))
    {
        return E_INVALIDARG;
    }

    this->device->sessionInfo.volumes[nChannel] = fLevel;

    HRESULT status;
    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}

/* The SetMasterVolumeLevel method sets the master volume level, in decibels, of
 * the audio stream that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::SetMasterVolumeLevel(float fLevelDB,
    LPCGUID pguidEventContext)
{
    if ((fLevelDB < this->device->sessionInfo.minDB)
        || (fLevelDB > this->device->sessionInfo.maxDB))
    {
        return E_INVALIDARG;
    }

    float levelScalar = std::pow(10, fLevelDB / this->device->sessionInfo.dBFactor);

    this->device->sessionInfo.masterVolume = levelScalar;

    HRESULT status;
    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}

/* The SetMasterVolumeLevelScalar method sets the master volume level of the
 * audio stream that enters or leaves the audio endpoint device. The volume
 * level is expressed as a normalized, audio-tapered value in the range from
 * 0.0 to 1.0. */
HRESULT IAudioEndpointVolumeEmulator::SetMasterVolumeLevelScalar(float fLevel,
    LPCGUID pguidEventContext)
{
    if ((fLevel < 0.0f) || (fLevel > 1.0f))
    {
        return E_INVALIDARG;
    }

    this->device->sessionInfo.masterVolume = fLevel;

    HRESULT status;
    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}

/* The SetMute method sets the muting state of the audio stream that enters or
 * leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::SetMute(BOOL bMute,
    LPCGUID pguidEventContext)
{
    if (this->device->sessionInfo.muted == bMute)
    {
        return S_FALSE;
    }

    this->device->sessionInfo.muted = bMute;

    HRESULT status;
    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}


/* The UnregisterControlChangeNotify method deletes the registration of a
 * client's notification callback interface that the client registered in a
 * previous call to the IAudioEndpointVolume::RegisterControlChangeNotify
 * method. */
HRESULT IAudioEndpointVolumeEmulator::UnregisterControlChangeNotify(
   IAudioEndpointVolumeCallback * pNotify)
{
    if (pNotify == NULL)
    {
        return E_POINTER;
    }

    HRESULT result = this->device->sessionInfo.removeListener(pNotify);
    if (result != S_OK)
    {
        return result;
    }

    pNotify->Release();

    return S_OK;
}

/* The VolumeStepDown method decrements, by one step, the volume level of the
 * audio stream that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::VolumeStepDown(LPCGUID pguidEventContext)
{
    HRESULT status = S_OK;

    UINT step = 0;
    UINT stepCount = 0;

    status = this->GetVolumeStepInfo(&step, &stepCount);
    if (status != S_OK)
    {
        return E_FAIL;
    }

    step--;

    if (step <= 0)
    {
        this->device->sessionInfo.masterVolume = 0.0f;
    }
    else
    {
        float newDBValue = step * this->device->sessionInfo.volumeIncrementDB;
        float newScalarValue = std::pow(10,
            newDBValue / this->device->sessionInfo.dBFactor);

        this->device->sessionInfo.masterVolume = newScalarValue;
    }

    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}

/* The VolumeStepUp method increments, by one step, the volume level of the
 * audio stream that enters or leaves the audio endpoint device. */
HRESULT IAudioEndpointVolumeEmulator::VolumeStepUp(LPCGUID pguidEventContext)
{
    HRESULT status = S_OK;

    UINT step = 0;
    UINT stepCount = 0;

    status = this->GetVolumeStepInfo(&step, &stepCount);
    if (status != S_OK)
    {
        return E_FAIL;
    }

    /* Clamp that */
    step++;

    if (step >= stepCount)
    {
        this->device->sessionInfo.masterVolume = 1.0f;
    }
    else
    {
        float newDBValue = step * this->device->sessionInfo.volumeIncrementDB;
        float newScalarValue = std::pow(10,
            newDBValue / this->device->sessionInfo.dBFactor);

        this->device->sessionInfo.masterVolume = newScalarValue;
    }

    status = this->device->sessionInfo.notifyChangeListeners(pguidEventContext);
    if(status != S_OK)
    {
        return status;
    }

    return S_OK;
}
