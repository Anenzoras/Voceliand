/*
 *  File:   IMMDeviceEmulator.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-14
 *
 *  Brief:  Implementation of the class emulating an IMMDevice interface.
 */

/* External library include */
#include <propsys.h> /* PROPVARIANT, PROPERTYKEY */
#include <Functiondiscoverykeys_devpkey.h> /* PKEY_DeviceInterface_FriendlyName,
                                            * PKEY_Device_DeviceDesc,
                                            * PKEY_Device_FriendlyName */
#include <devicetopology.h> /* IDeviceTopology */

/* Project includes */
#include "IMMDeviceEmulator.hpp"
#include "IAudioEndpointVolumeEmulator.hpp"
#include "IAudioClientEmulator.hpp"
#include "SharedAudioBuffer.hpp"
#include "Log.hpp"

/* https://docs.microsoft.com/en-us/windows/desktop/api/propkeydef/nf-propkeydef-define_propertykey */
#define DEFINE_PROPERTYKEY_PROXY(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8, pid) \
    EXTERN_C const PROPERTYKEY DECLSPEC_SELECTANY name = \
    { { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }, pid }

/* Because those two can not be found in our devpkey.h we define them here
 * based on https://github.com/pauldotknopf/PortAudio/blob/master/src/hostapi/wasapi/mingw-include/FunctionDiscoveryKeys_devpkey.h */
DEFINE_PROPERTYKEY_PROXY(PKEY_DeviceInterface_FriendlyName, 0x026e516e, 0xb814,
    0x414b, 0x83, 0xcd, 0x85, 0x6d, 0x6f, 0xef, 0x48, 0x22,
    2); // DEVPROP_TYPE_STRING
DEFINE_PROPERTYKEY_PROXY(PKEY_Device_DeviceDesc, 0xa45c254e, 0xdf1c, 0x4efd,
    0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 2); // DEVPROP_TYPE_STRING

/* And those are because linking is actually hell.
 * Same reason we redefine GUID in files.
 * TODO: We should actually use initgui.h header. We should make a helper header
 * one day. */
DEFINE_PROPERTYKEY_PROXY(PKEY_Device_FriendlyName, 0xa45c254e, 0xdf1c, 0x4efd,
    0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 14); // DEVPROP_TYPE_STRING
DEFINE_PROPERTYKEY_PROXY(PKEY_Device_LocationInfo, 0xa45c254e, 0xdf1c, 0x4efd,
    0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 15); // DEVPROP_TYPE_STRING
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_Association, 0x1da5d803, 0xd492,
    0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 2);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_ControlPanelPageProvider,
    0x1da5d803, 0xd492, 0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e,
    1);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_Disable_SysFx, 0x1da5d803, 0xd492,
    0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 5);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_FormFactor, 0x1da5d803, 0xd492,
    0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 0);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_FullRangeSpeakers, 0x1da5d803,
    0xd492, 0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 6);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_GUID, 0x1da5d803, 0xd492, 0x4edd,
    0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 4);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_PhysicalSpeakers, 0x1da5d803,
    0xd492, 0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e, 3);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEngine_DeviceFormat, 0xf19f064d, 0x82c,
    0x4e27, 0xbc, 0x73, 0x68, 0x82, 0xa1, 0xbb, 0x8e, 0x4c, 0);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEngine_OEMFormat, 0xe4870e26, 0x3cc5, 0x4cd2,
    0xba, 0x46, 0xca, 0x0a, 0x9a, 0x70, 0xed, 0x04, 3);
DEFINE_PROPERTYKEY_PROXY(PKEY_AudioEndpoint_Supports_EventDriven_Mode,
    0x1da5d803, 0xd492, 0x4edd, 0x8c, 0x23, 0xe0, 0xc0, 0xff, 0xee, 0x7f, 0x0e,
    7);

#undef DEFINE_PROPERTYKEY_PROXY

/* Because linking is fucking crap */
/* More info: https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/ks/ns-ks-kspin_descriptor
 * String value of the GUID:
 *  KSNODETYPE_MICROPHONE = "{DFF21BE1-F70F-11D0-B917-00A0C9223196}"
 * from: https://www.freelists.org/post/wdmaudiodev/Help-installing-custom-sAPO-for-a-USB-audio-device,6
 * GUID struct:
 *  typedef struct _GUID {
 *      DWORD Data1;
 *      WORD  Data2;
 *      WORD  Data3;
 *      BYTE  Data4[8];
 *  } GUID;
 * from: https://msdn.microsoft.com/en-us/library/windows/desktop/aa373931(v=vs.85).aspx
 */
GUID const KSNODETYPE_MICROPHONE = {0xDFF21BE1, 0xF70F, 0x11D0,
    {0xB9, 0x17, 0x00, 0xA0, 0xC9, 0x22, 0x31, 0x96}};

/* The unique identifier for the IMMDevice interface. */
IID const IID_IMMDevice = __uuidof(IMMDevice);
IID const IID_IMMEndpoint = __uuidof(IMMEndpoint);

/**
 *  @class IPropertyStoreEmulator
 *      Emulator for an IPropertyStore class we can access.
 *      Following microsoft's documentation, this class must allow acces to
 *      several properties:
 *      - https://docs.microsoft.com/fr-fr/windows/desktop/CoreAudio/device-properties
 *      - https://docs.microsoft.com/en-us/windows/desktop/coreaudio/audio-endpoint-properties
 */
class IPropertyStoreEmulator
    : public IPropertyStore
{
public:
    /* IPropertyStoreEmulator stuff */

    static IPropertyStoreEmulator * CreateInstance(DWORD stgmAccess)
    {
        IPropertyStoreEmulator * dummy = new IPropertyStoreEmulator(stgmAccess);
        if (dummy == NULL)
        {
            return NULL;
        }
        if (!dummy->valid)
        {
            delete dummy;

            return NULL;
        }

        dummy->AddRef();

        return dummy;
    }

    /**
     *  @function ~IPropertyStoreEmulator
     *      Destructor. Cleanly deletes the propvariant structures.
     */
    virtual ~IPropertyStoreEmulator()
    {
        dbglog->write("Enter IPropertyStoreEmulator::~IPropertyStoreEmulator()\n");

        HRESULT result = S_OK;

        for (size_t i = 0; i < PSE_Property_COUNT; i++)
        {
            dbglog->write("    %u\n", i);

            result = PropVariantClear(&this->propertyPairs[i].pvar);
            if (result != S_OK)
            {
                dbglog->write("IPropertyStoreEmulator::~IPropertyStoreEmulator()"
                    " Could not clear property of index %u\n", i);
            }
        }

        dbglog->write("Exit IPropertyStoreEmulator::~IPropertyStoreEmulator()\n");
    }

    /**
     *  @function Commit
     *      After a change has been made, this method saves the changes.
     *
     *  @returns {HRESULT}
     *      S_OK if all of the property changes were successfully written to the
     *          stream or path. This includes the case where no changes were
     *          pending when the method was called and nothing was written.
     *      STG_E_ACCESSDENIED if the stream or file is read-only; the method
     *          was not able to set the value.
     *      E_FAIL if some or all of the changes could not be written to the file.
     *          Another, more explanatory error can be used in place of E_FAIL.
     */
    HRESULT STDMETHODCALLTYPE Commit()
    {
        if (this->stgmAccess == STGM_READ)
        {
            return STG_E_ACCESSDENIED;
        }

        /* Return S_OK anyway, even if we do not modify */
        return S_OK;
    }

    /**
     *  @function GetAt
     *      Gets a property key from the property array of an item.
     *
     *  @param {DWORD} iProp
     *      The index of the property key in the array of PROPERTYKEY
     *      structures. This is a zero-based index.
     *  @param {PROPERTYKEY *} pkey
     *      From microsoft documentation: "TBD"
     *      From my understanding: a non-null pointer to a PROPERTYKEY
     *      structure.
     *      When this method returns, contains a PROPERTYKEY structure that
     *      receives the unique identifier for a property.
     *
     *  @returns {HRESULT}
     *      S_OK if the method is successful
     *      Otherwise, any other code it returns must be considered to be an
     *      error code.
     *      (Undocumented:) E_INVALIDARG if the paramter iProp is greater than
     *          the value returned by IPropertyStoreEmulator::GetCount().
     *      (Undocumented:) E_POINTER if the paramter pkey is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetAt(DWORD iProp, PROPERTYKEY * pkey)
    {
        if (pkey == NULL)
        {
            return E_POINTER;
        }

        DWORD count = 0;

        if (S_OK != this->GetCount(&count))
        {
            return E_FAIL;
        }

        if (iProp >= count)
        {
            return E_INVALIDARG;
        }

        (*pkey) = (this->propertyPairs[iProp].pkey);

        return S_OK;
    }

    /**
     *  @function GetCount
     *      This method returns a count of the number of properties that are
     *      attached to the file.
     *
     *  @param {DWORD * } cProps
     *      A pointer to a value that indicates the property count.
     *
     *  @returns {HRESULT}
     *      S_OK if the method is successful, even if the file has no properties
     *      attached. Any other code returned is an error code.
     *      (Undocumented:) E_POINTER if cProps is NULL
     */
    HRESULT STDMETHODCALLTYPE GetCount(DWORD * cProps)
    {
        if (cProps == NULL)
        {
            return E_POINTER;
        }

        (*cProps) = PSE_Property_COUNT;

        return S_OK;
    }

    /**
     *  @function GetValue
     *      This method retrieves the data for a specific property.
     *
     *  @param {REFPROPERTYKEY} key
     *      Official doc: TBD
     *      My own doc: The property key identifying the property to access.
     *  @param {PROPVARIANT *} pv
     *      After the IPropertyStore::GetValue method returns successfully, this
     *      parameter points to a PROPVARIANT structure that contains data about
     *      the property.
     *      (My own doc: the struct is not created/initialized by us.)
     *
     *  @returns {HRESULT}
     *      S_OK if the method is successful
     *      INPLACE_S_TRUNCATED is the method is successful and indicates that
     *          the returned PROPVARIANT was converted into a more canonical
     *          form. For example, this would be done to trim leading or
     *          trailing spaces from a string value. You must use the SUCCEEDED
     *          macro to check the return value, which treats
     *          INPLACE_S_TRUNCATED as a success code. The SUCCEEDED macro is
     *          defined in the Winerror.h file.
     *      (Undocumented:) E_POINTER if pv is NULL.
     *      Note: if the PROPERTYKEY referenced in key is not present in the
     *      property store, this method returns S_OK and the vt member of the
     *      structure pointed to by pv is set to VT_EMPTY.
     */
    HRESULT STDMETHODCALLTYPE GetValue(REFPROPERTYKEY key, PROPVARIANT * pv)
    {
        dbglog->write("Enter IPropertyStoreEmulator::GetValue()\n");

        if (pv == NULL)
        {
            return E_POINTER;
        }

        int32_t index = getPropIndex(key);

        if (index < 0)
        {
            pv->vt = VT_EMPTY;

            return S_OK;
        }

        PropVariantCopy(pv, &this->propertyPairs[index].pvar);

        dbglog->write("Exit IPropertyStoreEmulator::GetValue()\n");

        return S_OK;
    }

    /**
     *  @function SetValue
     *      This method sets a property value or replaces or removes an existing
     *      value.
     *
     *
     *
     *  @returns {HRESULT}
     *      S_OK if the property change was successful.
     *          (Addition:) Based on the GetValue() documentation, if the
     *          property referenced by key is not present, return S_OK but does
     *          nothing (probably not wanted TODO).
     *      INPLACE_S_TRUNCATED if the value was set but truncated.
     *      STG_E_ACCESSDENIED if the property store was read-only so the method
     *          was not able to set the value.
     */
    HRESULT STDMETHODCALLTYPE SetValue(REFPROPERTYKEY key,
        REFPROPVARIANT propvar)
    {
        if (this->stgmAccess == STGM_READ)
        {
            return STG_E_ACCESSDENIED;
        }

        int32_t index = getPropIndex(key);

        if (index < 0)
        {
            return S_OK;
        }

        if (index == PSE_Property_AUDIOENDPOINT_GUID)
        {
            /* "PKEY_AudioEndpoint_GUID is a read-only property regardless of
             * the storage-access mode requested by the application in
             * IMMDevice::OpenPropertyStore. If an application attempts to set a
             * value by using IPropertyStore::SetValue, this call fails with the
             * E_ACCESSDENIED error code." */
            return E_ACCESSDENIED;
        }

        PropVariantClear(&this->propertyPairs[index].pvar);
        PropVariantCopy(&this->propertyPairs[index].pvar, &propvar);

        return S_OK;
    }

    ULONG STDMETHODCALLTYPE AddRef()
    {
        InterlockedIncrement(&this->referenceCount);

        return this->referenceCount;
    }

    ULONG STDMETHODCALLTYPE Release()
    {
        ULONG ref = InterlockedDecrement(&this->referenceCount);
        if (this->referenceCount == 0)
        {
            /* End of life */
            delete this;
        }

        return ref;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid,
        void ** ppvObject)
    {
        if (ppvObject == NULL)
        {
            return E_POINTER;
        }
        *ppvObject = NULL;

        if (IsEqualIID(riid, IID_IUnknown)
            || IsEqualIID(riid, IID_IPropertyStore))
        {
            /* Increment the reference count and return the pointer. */
            *ppvObject = (LPVOID)this;
            this->AddRef();

            return S_OK;
        }
        else
        {
            return E_NOINTERFACE;
        }
    }

private:
    /* IPropertyStoreEmulator private stuff */

    /**
     *  @struct _PropertyPair_s
     *      Is a pair of PROPERTYKEY / PROPVARIANT, because fuck that shit.
     */
    struct _PropertyPair_s
    {
        PROPERTYKEY pkey;
        PROPVARIANT pvar;
    };

    /**
     *  @enum _Property_e
     *      Small enum for quick access to properties.
     */
    enum _Property_e
    {
        PSE_Property_DEVICEINTERFACE_FRIENDLYNAME = 0,
        PSE_Property_DEVICE_DEVICEDESC,
        PSE_Property_DEVICE_FRIENDLYNAME,
        PSE_Property_AUDIOENDPOINT_ASSOCIATION,
        PSE_Property_AUDIOENDPOINT_CONTROLPANELPAGEPROVIDER,
        PSE_Property_AUDIOENDPOINT_DISABLE_SYSFX,
        PSE_Property_AUDIOENDPOINT_FORMFACTOR,
        PSE_Property_AUDIOENDPOINT_FULLRANGESPEAKERS,
        PSE_Property_AUDIOENDPOINT_GUID,
        PSE_Property_AUDIOENDPOINT_PHYSICALSPEAKERS,
        PSE_Property_AUDIOENGINE_DEVICEFORMAT,
        PSE_Property_AUDIOENGINE_OEMFORMAT,
        PSE_Property_AUDIOENDPOINT_SUPPORTS_EVENTDRIVEN_MODE,

        PSE_Property_COUNT
    };

    /**
     *  @property {bool} valid
     *      Keeps track of the validity of the object during creation.
     *      CreateInstance() should return NULL upon creation if valid is FALSE.
     */
    bool valid = FALSE;

    /**
     *  @property {DWORD} stgmAccess
     *      Access rights. Can be STGM_READ, STGM_WRITE or STGM_READWRITE.
     *      Only possible access as non administrator is STGM_READ.
     *
     *      I believe we will never have another access right, but...
     */
    DWORD stgmAccess = STGM_READ;

    /**
     *  @property {_PropertyPair_s[]} propertyPairs
     *      Contains the pairs of property keys and their values as a dumb
     *      static array.
     */
    _PropertyPair_s propertyPairs[PSE_Property_COUNT];

    /**
     *  @function getPropIndex
     *      Returns the index of the property
     *
     *  @param {PROPERTYKEY} key
     *      The propertykey allowing to retrieve the property.
     *
     *  @returns {int32_t}
     *      The index of the property, if found.
     *      -1 otherwise.
     */
    int32_t getPropIndex(PROPERTYKEY key)
    {
        for (int32_t i = 0; i < PSE_Property_COUNT; i++)
        {
            PROPERTYKEY pkey = (this->propertyPairs[i].pkey);

            if (IsEqualPropertyKey(pkey, key))
            {
                return i;
            }
        }

        return -1;
    }

    /**
     *  @function IPropertyStoreEmulator
     *      Constructor. Private for reasons.
     */
    IPropertyStoreEmulator(DWORD stgmAccess)
    {
        dbglog->write("Enter IPropertyStoreEmulator()\n");

        this->referenceCount = 0;
        this->stgmAccess = stgmAccess;

        /* Init our thingies */

        /* First pass to init everything. */
        for (size_t i = 0; i < PSE_Property_COUNT; i++)
        {
            PropVariantInit(&this->propertyPairs[i].pvar);
        }

        /* Just to avoid a line to be billions of char long */
        size_t index = 0;

        {
            wchar_t const * interfaceFriendlyName = L"Voceliand Device";
            size_t length = wcslen(interfaceFriendlyName);

            wchar_t * tmp = static_cast<LPWSTR>(CoTaskMemAlloc(sizeof(wchar_t)
                * (length + 1)));
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }
            wcscpy(tmp, interfaceFriendlyName);

            index = PSE_Property_DEVICEINTERFACE_FRIENDLYNAME;

            this->propertyPairs[index].pkey = PKEY_DeviceInterface_FriendlyName;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            wchar_t const * description = L"Emulated Microphone";
            size_t length = wcslen(description);
            wchar_t * tmp = static_cast<LPWSTR>(CoTaskMemAlloc(sizeof(wchar_t)
                * (length + 1)));
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            wcscpy(tmp, description);

            index = PSE_Property_DEVICE_DEVICEDESC;

            this->propertyPairs[index].pkey = PKEY_Device_DeviceDesc;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            wchar_t const * friendlyName = L"Emulated Microphone (Voceliand"
                " Device)";
            size_t length = wcslen(friendlyName);
            wchar_t * tmp = static_cast<LPWSTR>(CoTaskMemAlloc(sizeof(wchar_t)
                * (length + 1)));
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            wcscpy(tmp, friendlyName);

            index = PSE_Property_DEVICE_FRIENDLYNAME;

            this->propertyPairs[index].pkey = PKEY_Device_FriendlyName;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            wchar_t * tmp;
            StringFromCLSID(KSNODETYPE_MICROPHONE, &tmp);
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            index = PSE_Property_AUDIOENDPOINT_ASSOCIATION;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_Association;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            wchar_t const * controlPanelPageProvider = L""; /* TODO, migth cause error */
            size_t length = wcslen(controlPanelPageProvider);
            wchar_t * tmp = static_cast<LPWSTR>(CoTaskMemAlloc(sizeof(wchar_t)
                * (length + 1)));
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            wcscpy(tmp, controlPanelPageProvider);

            index = PSE_Property_AUDIOENDPOINT_CONTROLPANELPAGEPROVIDER;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_ControlPanelPageProvider;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            index = PSE_Property_AUDIOENDPOINT_DISABLE_SYSFX;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_Disable_SysFx;
            this->propertyPairs[index].pvar.vt = VT_UI4;
            this->propertyPairs[index].pvar.uintVal = ENDPOINT_SYSFX_DISABLED;
            /* According to documentation, it should be
             * this->propertyPairs[index].pvar.ulval = ENDPOINT_SYSFX_DISABLED;
             * but it doesn't compile (?) and also doesn't really make sense */
        }

        {
            index = PSE_Property_AUDIOENDPOINT_FORMFACTOR;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_FormFactor;
            this->propertyPairs[index].pvar.vt = VT_UI4;
            this->propertyPairs[index].pvar.uintVal = Microphone;
            /* EndpointFormFactor::Microphone */
        }

        {
            index = PSE_Property_AUDIOENDPOINT_FULLRANGESPEAKERS;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_FullRangeSpeakers;
            this->propertyPairs[index].pvar.vt = VT_UI4;
            this->propertyPairs[index].pvar.uintVal = KSAUDIO_SPEAKER_STEREO;
            /* https://msdn.microsoft.com/en-us/library/windows/desktop/dd390971(v=vs.85).aspx */
        }

        {
            wchar_t const * audioEndpointGUID = L""; /* TODO, migth cause error */
            size_t length = wcslen(audioEndpointGUID);
            wchar_t * tmp = static_cast<LPWSTR>(CoTaskMemAlloc(sizeof(wchar_t)
                * (length + 1)));
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            wcscpy(tmp, audioEndpointGUID);

            index = PSE_Property_AUDIOENDPOINT_GUID;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_GUID;
            this->propertyPairs[index].pvar.vt = VT_LPWSTR;
            this->propertyPairs[index].pvar.pwszVal = tmp;
        }

        {
            index = PSE_Property_AUDIOENDPOINT_PHYSICALSPEAKERS;

            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_PhysicalSpeakers;
            this->propertyPairs[index].pvar.vt = VT_UI4;
            this->propertyPairs[index].pvar.uintVal = KSAUDIO_SPEAKER_STEREO;
        }

        {
            WAVEFORMATEX * tmp = newDefaultFormat();
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            index = PSE_Property_AUDIOENGINE_DEVICEFORMAT;
            this->propertyPairs[index].pkey = PKEY_AudioEngine_DeviceFormat;
            this->propertyPairs[index].pvar.vt = VT_BLOB;
            this->propertyPairs[index].pvar.blob.cbSize = sizeof(WAVEFORMATEX);
            this->propertyPairs[index].pvar.blob.pBlobData = (BYTE *) tmp;
            /* Hard cast because meh */
        }

        {
            WAVEFORMATEX * tmp = newDefaultFormat();
            if (tmp == NULL)
            {
                this->valid = FALSE;
                return;
            }

            index = PSE_Property_AUDIOENGINE_OEMFORMAT;
            this->propertyPairs[index].pkey = PKEY_AudioEngine_OEMFormat;
            this->propertyPairs[index].pvar.vt = VT_BLOB;
            this->propertyPairs[index].pvar.blob.cbSize = sizeof(WAVEFORMATEX);
            this->propertyPairs[index].pvar.blob.pBlobData = (BYTE *) tmp;
            /* Hard cast because meh */
        }

        {
            index = PSE_Property_AUDIOENDPOINT_SUPPORTS_EVENTDRIVEN_MODE;
            this->propertyPairs[index].pkey = PKEY_AudioEndpoint_Supports_EventDriven_Mode;
            this->propertyPairs[index].pvar.vt = VT_UI4;
            this->propertyPairs[index].pvar.uintVal = FALSE;
        }

        this->valid = TRUE;

        dbglog->write("Exit IPropertyStoreEmulator()\n");
    }

    /**
     *  @property {ULONG} referenceCount
     *      Used by IUnkown implementation.
     */
    ULONG referenceCount;
};

/*
 *  IMMDeviceEmulator stuff
 */
/* Returns an instance of IMMDeviceEmulator to encapsulate thingies. */
IMMDeviceEmulator * IMMDeviceEmulator::CreateInstance(void)
{
    IMMDeviceEmulator * emulator = new IMMDeviceEmulator();
    if (emulator == NULL)
    {
        return NULL;
    }

    emulator->AddRef();

    return emulator;
}

/* Constructor. Private for reasons. */
IMMDeviceEmulator::IMMDeviceEmulator(void)
{
    this->referenceCount = 0;
}

/* Destructor. */
IMMDeviceEmulator::~IMMDeviceEmulator()
{
}

/*
 * IUnknown stuff
 */
/* Increments the reference count for an interface on an object. This method
 * should be called for every new copy of a pointer to an interface on an
 * object. */
ULONG IMMDeviceEmulator::AddRef()
{
    InterlockedIncrement(&this->referenceCount);

    return this->referenceCount;
}

/* Decrements the reference count for an interface on an object. */
ULONG IMMDeviceEmulator::Release()
{
    ULONG ref = InterlockedDecrement(&this->referenceCount);
    if (this->referenceCount == 0)
    {
        /* End of life */
        delete this;
    }

    return ref;
}

/* Retrieves pointers to the supported interfaces on an object. */
HRESULT IMMDeviceEmulator::QueryInterface(REFIID riid,
    void ** ppvObject)
{
    dbglog->write("Enter QueryInterface()\n");

    if (ppvObject == NULL)
    {
        return E_POINTER;
    }

    (*ppvObject) = NULL;

    HRESULT result = E_NOINTERFACE;

    if (IsEqualIID(riid, IID_IUnknown))
    {
        (*ppvObject) = this;

        result = S_OK;
    }
    else if (IsEqualIID(riid, IID_IMMDevice))
    {
        (*ppvObject) = static_cast<IMMDevice *>(this);

        result = S_OK;
    }
    else if (IsEqualIID(riid, IID_IMMEndpoint))
    {
        (*ppvObject) = static_cast<IMMEndpoint *>(this);

        result = S_OK;
    }

    if (result == S_OK)
    {
        this->AddRef();
    }

    return result;
}

/*
 * IMMDevice stuff
 */
 /* The Activate method creates a COM object with the specified interface. */
HRESULT IMMDeviceEmulator::Activate(REFIID iid, DWORD dwClsCtx,
    PROPVARIANT * pActivationParams, void ** ppInterface)
{
    dbglog->write("Enter IMMDeviceEmulator::Activate()\n");


    if (ppInterface == NULL)
    {
        return E_POINTER;
    }

    if (/* IsEqualIID(idd, IID_IAudioClient) && */ (pActivationParams != NULL))
    {
        return E_INVALIDARG;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    /* TODO: I have no idea on what to do with dwClsCtx. This may results in
     * bugs, make sure it does not.
     * -Anenzoras, 2019-02-16
     */
    (void) dwClsCtx;

    HRESULT result = E_NOINTERFACE;

    if (IsEqualIID(iid, IID_IAudioClient))
    {
        dbglog->write("    IAudioClient\n");

        (*ppInterface) = IAudioClientEmulator::CreateInstance(this);
        if ((*ppInterface) == NULL)
        {
            return E_OUTOFMEMORY;
        }

        result = S_OK;
    }
    else if(IsEqualIID(iid, IID_IAudioEndpointVolume))
    {
        dbglog->write("    IAudioEndpointVolume\n");

        (*ppInterface) = IAudioEndpointVolumeEmulator::CreateInstance(this);
        if ((*ppInterface) == NULL)
        {
            return E_OUTOFMEMORY;
        }

        result = S_OK;
    }
    else
    {
        wchar_t * iidString = NULL;
        wchar_t * iidIACString = NULL;
        StringFromCLSID(iid, &iidString);
        StringFromCLSID(IID_IAudioClient, &iidIACString);

        fwprintf(dbglog->file, L"%ls\n", iidString);
        fwprintf(dbglog->file, L"%ls\n", iidIACString);

        CoTaskMemFree(iidString);
        CoTaskMemFree(iidIACString);

        dbglog->write("    E_NOINTERFACE\n");
    }

    /*  TODO: Make sure we don't need to cover more interfaces
     *  -Anenzoras, 2019-03-18 */

    dbglog->write("Exit IMMDeviceEmulator::Activate()\n");

    return result;
}

/* The GetId method retrieves an endpoint ID string that identifies the audio
 * endpoint device. */
HRESULT IMMDeviceEmulator::GetId(LPWSTR * ppstrId)
{
    dbglog->write("Enter IMMDeviceEmulator::GetId()\n");

    if (ppstrId == NULL)
    {
        return E_POINTER;
    }

    size_t length = wcslen(IMMDeviceEmulator_LDEVICE_ID);
    (*ppstrId) = static_cast<LPWSTR>(
        CoTaskMemAlloc(sizeof(wchar_t) * (length + 1))); /* +1 for the \0 */
    if ((*ppstrId) == NULL)
    {
        return E_OUTOFMEMORY;
    }

    wcscpy((*ppstrId), IMMDeviceEmulator_LDEVICE_ID);

    dbglog->write("Exit IMMDeviceEmulator::GetId()\n");

    return S_OK;
}

/* The GetState method retrieves the current device state. */
HRESULT IMMDeviceEmulator::GetState(DWORD * pdwState)
{
    dbglog->write("Enter IMMDeviceEmulator::GetState()\n");

    if (pdwState == NULL)
    {
        return E_POINTER;
    }

    if (sharedBuffer.connected)
    {
        (*pdwState) = DEVICE_STATE_ACTIVE;
    }
    else
    {
        (*pdwState) = DEVICE_STATE_DISABLED;
    }

    dbglog->write("Exit IMMDeviceEmulator::GetState()\n");

    /* TODO:
    DEVICE_STATE_NOTPRESENT
    DEVICE_STATE_UNPLUGGED */

    return S_OK;
}

/* The OpenPropertyStore method retrieves an interface to the device's property
 * store. */
HRESULT IMMDeviceEmulator::OpenPropertyStore(DWORD stgmAccess,
    IPropertyStore ** ppProperties)
{
    dbglog->write("Enter IMMDeviceEmulator::OpenPropertyStore()\n");

    /* Errors first */
    if ((stgmAccess != STGM_READ)
        && (stgmAccess != STGM_WRITE)
        && (stgmAccess != STGM_READWRITE))
    {
        return E_INVALIDARG;
    }

    if (ppProperties == NULL)
    {
        return E_POINTER;
    }

    (*ppProperties) = IPropertyStoreEmulator::CreateInstance(stgmAccess);
    if ((*ppProperties) == NULL)
    {
        return E_OUTOFMEMORY;
    }

    dbglog->write("Exit IMMDeviceEmulator::OpenPropertyStore()\n");

    return S_OK;
}

/* The GetDataFlow method indicates whether the audio endpoint device is a
 * rendering device or a capture device. */
HRESULT IMMDeviceEmulator::GetDataFlow(EDataFlow * pDataFlow)
{
    dbglog->write("Enter IMMDeviceEmulator::GetDataFlow()\n");

    if (pDataFlow == NULL)
    {
        return E_POINTER;
    }

    (*pDataFlow) = eCapture;

    dbglog->write("Exit IMMDeviceEmulator::GetDataFlow()\n");

    return S_OK;
}
