/*
 *  File:   SharedAudioBuffer.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-19
 *
 *  Brief:  Implementation of the interprocess sharing class.
 */

/* External library includes */
#include <windows.h>

/* Project includes */
#include "SharedAudioBuffer.hpp"

SharedAudioBuffer sharedBuffer;

float getCurrentTime(void)
{
    static LARGE_INTEGER frequency;

    QueryPerformanceFrequency(&frequency);

    LARGE_INTEGER time;

    // Get the current time
    QueryPerformanceCounter(&time);

    // // Return the current time as microseconds
    // return Time::inMicroseconds(1000000 * time.QuadPart / frequency.QuadPart);
    // Return the current time as seconds
    return ((float) time.QuadPart / (float) frequency.QuadPart);
}
