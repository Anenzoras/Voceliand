/*
 *  File:   SoundHelpers.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 *  Note:   SoundHelpers is a private file to help dealing with sound buffers.
 */

/* standard library includes */
#include <inttypes.h>
#include <vector>

/* external library includes */
/* project includes */
#include "../../include/Audio/AudioConfiguration.hpp"

namespace Voceliand
{
    namespace SoundHelpers
    {
        namespace
        {
            /* To access the second part of the following lookup table. */
            size_t const SIGNED = 0;
            size_t const UNSIGNED = 1;

            /* Let us avoid a switch-case. -MEH- */
            // double const LOOKUP_MAX_D[4][2] =
            // {
            //     {127.0, 255.0},
            //     {32767.0, 65535.0},
            //     {8388607.0, 16777215.0},
            //     {2147483647.0, 4294967295.0}
            // };
            double const LOOKUP_MAX_D[4] =
            {
                128.0,
                32768.0,
                8388608.0,
                2147483648.0
            };

            union transcoderF32_u
            {
                float value;
                char  code[4];
            };
            union transcoderF64_u
            {
                double value;
                char   code[8];
            };
            union transcoderI64_u
            {
                uint64_t value;
                char     code[8];
            };

            inline void copy(char * from, char * to, uint32_t const size,
                bool reverse)
            {
                for (size_t i = 0; i < size; i++)
                {
                    /* !reverse: to[i] = from[i]
                     * reverse: to[i] = from[size - i]
                     * what not to do to avoid ifs, eh? */
                    to[i] = from[(size * reverse) - (2 * i * reverse) + i];
                }
            }

            /* Helper to access a sample in the array of char. */
            inline void getAt(std::vector<char> const & samples, size_t index,
                char sample[8])
            {
                uint32_t sampleSize = AudioConfiguration::sampleInfo.size;

                for (size_t i = 0; i < sampleSize; i++)
                {
                    sample[i] = samples[index * sampleSize + i];
                }
            }

            /* Helper to set a sample in the array of char. */
            inline void setAt(char const sample[8], std::vector<char> & samples,
                size_t index)
            {
                uint32_t sampleSize = AudioConfiguration::sampleInfo.size;

                for (size_t i = 0; i < sampleSize; i++)
                {
                    samples[index * sampleSize + i] = sample[i];
                }
            }
        }; /* namespace <anonymous> */

        /* This function takes an input vector of char whose carry data as
         * defined in AudioConfiguration::sampleInfo and transform it in a
         * vector of char holding floating point values. */
        void toFloatSamples(std::vector<char> const & samples,
            std::vector<double> & translatedSamples)
        {
            /* Clarity. */
            AudioConfiguration::SampleInfo const & sampleInfo = AudioConfiguration::sampleInfo;

            /* Safety first. */
            translatedSamples.resize(samples.size() / sampleInfo.size);

            for (size_t i = 0; i < translatedSamples.size(); i++)
            {
                /* The sample value as an array of char. */
                char sample[8];

                /* Fill the array */
                getAt(samples, i, sample);

                /* And now, depending on the configuration, translate to
                 * double. */
                if(sampleInfo.format == AudioConfiguration::Format_FLOAT)
                {
                    if (sampleInfo.size == sizeof(float))
                    {
                        transcoderF32_u transcoder;

                        copy(sample, transcoder.code, sampleInfo.size,
                            sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);

                        translatedSamples[i]
                            = static_cast<double>(transcoder.value);
                    }
                    else /* if (size == sizeof(double)) */
                    {
                        transcoderF64_u transcoder;

                        copy(sample, transcoder.code, sampleInfo.size,
                            sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);

                        translatedSamples[i] = transcoder.value;
                    }
                }
                else // if (format == XPCM)
                {
                    transcoderI64_u transcoder;
                    transcoder.value = 0;

                    copy(sample, transcoder.code, sampleInfo.size,
                        sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);

                    /* In case of unsigned integer, sign it. */
                    if (sampleInfo.format == AudioConfiguration::Format_UNSIGNED_PCM)
                    {
                        transcoder.value -= static_cast<int64_t>(LOOKUP_MAX_D[sampleInfo.size]);
                    }
                    else if((transcoder.value & (1 << (sampleInfo.bitDepth - 1))))
                    {
                        /* In case of signed integer, eventually propagate the
                         * sign. */
                        int64_t mask = 0xFFFFFFFFFFFFFFFF;

                        mask <<= (sampleInfo.bitDepth);

                        transcoder.value |= mask;
                    }

                    /* Translation to double, using a lookup table to avoid a
                     * switch. */
                    translatedSamples[i] = static_cast<double>(transcoder.value)
                        / LOOKUP_MAX_D[sampleInfo.size];
                }
            }
        }

        /* Complement to toFloatSamples(), retransform the given vector encoded
         * as floating point values back to the format corresponding to
         * AudioConfiguration::sampleInfo. */
        void reencode(std::vector<double> const & floatSamples,
            std::vector<char> & samples)
        {
            /* Clarity. */
            AudioConfiguration::SampleInfo const & sampleInfo = AudioConfiguration::sampleInfo;

            /* Safety first. */
            samples.resize(floatSamples.size() * sampleInfo.size);

            for (size_t i = 0; i < floatSamples.size(); i++)
            {
                /* The sample value as an array of char. */
                char sample[8];

                /* And now, depending on the configuration, translate to
                 * char. */
                if(sampleInfo.format == AudioConfiguration::Format_FLOAT)
                {
                    if (sampleInfo.size == sizeof(float))
                    {
                        transcoderF32_u transcoder;
                        transcoder.value = static_cast<float>(floatSamples[i]);

                        copy(transcoder.code, sample, sampleInfo.size,
                            sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);
                    }
                    else /* if (size == sizeof(double)) */
                    {
                        transcoderF64_u transcoder;
                        transcoder.value = floatSamples[i];

                        copy(transcoder.code, sample, sampleInfo.size,
                            sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);
                    }
                }
                else // if (format == XPCM)
                {
                    transcoderI64_u transcoder;
                    transcoder.value = static_cast<int64_t>(floatSamples[i]
                        * LOOKUP_MAX_D[sampleInfo.size]);
                    /* In case of unsigned integer, re-unsign it */
                    if(sampleInfo.format == AudioConfiguration::Format_UNSIGNED_PCM)
                    {
                        transcoder.value += static_cast<int64_t>(LOOKUP_MAX_D[sampleInfo.size]);
                    }

                    copy(transcoder.code, sample, sampleInfo.size,
                        sampleInfo.endianness == AudioConfiguration::Endianness_FOREIGN);
                }

                /* Fill the array */
                setAt(sample, samples, i);
            }
        }

        /* Access the sample in the given vector of sample within the given
         * channel and returns its value. */
        inline double getSampleOfChannelAt(std::vector<double> const & samples,
            size_t channel, size_t index)
        {
            uint32_t channelCount = AudioConfiguration::sampleInfo.channelCount;

            return samples[index * channelCount + channel];
        }

        /* Access the sample in the given vector of sample within the given
         * channel and returns a reference to it. */
        double getNthSampleOfChannel(std::vector<double> const & samples,
            size_t channel, size_t index)
        {
            uint32_t channelCount = AudioConfiguration::sampleInfo.channelCount;

            return samples[index * channelCount + channel];
        }

        /* Access the sample in the given vector of sample within the given
         * channel and sets its value to the given one. */
        void setNthSampleOfChannel(std::vector<double> & samples,
            size_t channel, size_t index, double value)
        {
            uint32_t channelCount = AudioConfiguration::sampleInfo.channelCount;

            samples[index * channelCount + channel] = value;
        }

        /* Returns the number of frame inside the given vector of samples. */
        size_t computeFrameCount(std::vector<double> const & samples)
        {
            uint32_t channelCount = AudioConfiguration::sampleInfo.channelCount;

            return (samples.size() / channelCount);
        }
    }; /* namespace SoundHelpers */
}; /* namespace Voceliand */
