/*
 *  File:   AudioFileWriterWAV.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 *  Note:   Temporary. This file writer will be used for tests before writing
 *      the actuel driver.
 */

/*
Mostly taken from SFML
https://github.com/SFML/SFML/blob/master/src/SFML/Audio/SoundFileWriterWav.cpp
*/

/* standard library includes */
#include <cassert>
#include <algorithm>
#include <iostream>

/* external library includes */
/* project includes */

#include "../include/AudioFileWriterWAV.hpp"

// The following function takes an array of char representing numbers in host
// byte order and writes them to a stream as little endian
namespace
{
    // void encode(std::ostream & stream, int16_t value)
    // {
    //     unsigned char bytes[] =
    //     {
    //         static_cast<unsigned char>(value & 0xFF),
    //         static_cast<unsigned char>(value >> 8)
    //     };
    //
    //     stream.write(reinterpret_cast<const char *>(bytes), sizeof(bytes));
    // }

    void encode(std::ostream & stream, uint16_t value)
    {
        unsigned char bytes[] =
        {
            static_cast<unsigned char>(value & 0xFF),
            static_cast<unsigned char>(value >> 8)
        };

        stream.write(reinterpret_cast<const char *>(bytes), sizeof(bytes));
    }

    void encode(std::ostream & stream, uint32_t value)
    {
        unsigned char bytes[] =
        {
            static_cast<unsigned char>(value & 0x000000FF),
            static_cast<unsigned char>((value & 0x0000FF00) >> 8),
            static_cast<unsigned char>((value & 0x00FF0000) >> 16),
            static_cast<unsigned char>((value & 0xFF000000) >> 24),
        };

        stream.write(reinterpret_cast<const char *>(bytes), sizeof(bytes));
    }

    // void encode(std::ostream & stream, char const * values, uint16_t sampleSize)
    // {
    //     #define SAMPLE_MAX_SIZE 8
    //     char buffer[SAMPLE_MAX_SIZE];
    //
    //     for (uint16_t i = 0; i < sampleSize; i++)
    //     {
    //         buffer[i] = values[(SAMPLE_MAX_SIZE - i) - 1];
    //     }
    //
    //     stream.write(reinterpret_cast<char const *>(buffer), sizeof(buffer));
    // }
};

namespace Voceliand
{
    /* translate format from soundio enum value to wav corresponding value */
    uint16_t translateFormat(SoundIoFormat const soundIoFormat)
    {
        if (soundIoFormat == SoundIoFormatFloat32NE
            || soundIoFormat == SoundIoFormatFloat32FE
            || soundIoFormat == SoundIoFormatFloat64NE
            || soundIoFormat == SoundIoFormatFloat64FE)
        {
            return AudioFileWriterWAV::FORMAT_FLOAT;
        }

        return AudioFileWriterWAV::FORMAT_PCM;
    }

    /* Check if this writer can handle a file on disk. */
    bool AudioFileWriterWAV::check(std::string const & filename)
    {
        std::string extension = filename.substr(filename.find_last_of(".") + 1);
        std::transform(extension.begin(), extension.end(), extension.begin(),
            ::tolower);

        return extension == "wav";
    }

    /* default constructor */
    AudioFileWriterWAV::AudioFileWriterWAV() :
    fileStream   (),
    sampleCount (0),
    channelCount(0)
    {
    }

    /* destructor */
    AudioFileWriterWAV::~AudioFileWriterWAV()
    {
        close();
    }

    /* Open a sound file for writing. */
    bool AudioFileWriterWAV::open(std::string const & filename,
        uint32_t sampleRate, uint16_t channelCount, uint16_t const audioFormat,
        uint16_t const sampleSize)
    {
        // Open the file
        this->fileStream.open(filename.c_str(), std::ios::binary);
        if (!this->fileStream)
        {
            std::cerr << "Failed to open WAV sound file \"" << filename
                << "\" for writing" << std::endl;
            return false;
        }

        this->sampleSize = sampleSize;

        // Write the header
        if (!writeHeader(sampleRate, channelCount, audioFormat))
        {
            std::cerr << "Failed to write header of WAV sound file \""
                << filename << "\"" << std::endl;
            return false;
        }

        // Save the channel count
        this->channelCount = channelCount;

        return true;
    }

    /* Write audio samples to the open file. */
    void AudioFileWriterWAV::write(char const * samples, uint64_t count)
    {
        assert(this->fileStream.good());

        this->sampleCount += count;

        while (count--)
        {
            this->fileStream.write(reinterpret_cast<char const *>(samples),
                    this->sampleSize);
            samples += this->sampleSize; // step up
        }
    }

    /* Write the header of the open file. */
    bool AudioFileWriterWAV::writeHeader(uint32_t sampleRate,
        uint16_t channelCount, uint16_t const audioFormat)
    {
        assert(this->fileStream.good());

        // Write the main chunk ID
        char mainChunkId[4] = {'R', 'I', 'F', 'F'};
        this->fileStream.write(mainChunkId, sizeof(mainChunkId));

        // Write the main chunk header
        uint32_t mainChunkSize = 0; // placeholder, will be written later
        encode(this->fileStream, mainChunkSize);
        char mainChunkFormat[4] = {'W', 'A', 'V', 'E'};
        this->fileStream.write(mainChunkFormat, sizeof(mainChunkFormat));

        // Write the sub-chunk 1 ("format") id and size
        char fmtChunkId[4] = {'f', 'm', 't', ' '};
        this->fileStream.write(fmtChunkId, sizeof(fmtChunkId));
        uint32_t fmtChunkSize = 16;
        encode(this->fileStream, fmtChunkSize);

        // Write the format (PCM (1) or float (3) depending on sample type)
        // and prepare bits per sample
        uint16_t format = audioFormat;
        encode(this->fileStream, format);

        // Write the sound attributes
        uint16_t bitsPerSample = this->sampleSize * 8;
        uint16_t blockAlign = channelCount * this->sampleSize;
        uint32_t byteRate = sampleRate * blockAlign;
        encode(this->fileStream, (channelCount));
        encode(this->fileStream, (sampleRate));
        encode(this->fileStream, byteRate);
        encode(this->fileStream, blockAlign);
        encode(this->fileStream, bitsPerSample);

        // Write the sub-chunk 2 ("data") id and size
        char dataChunkId[4] = {'d', 'a', 't', 'a'};
        this->fileStream.write(dataChunkId, sizeof(dataChunkId));
        uint32_t dataChunkSize = 0; // placeholder, will be written later
        encode(this->fileStream, dataChunkSize);

        return true;
    }

    /* Close the file */
    void AudioFileWriterWAV::close(void)
    {
        // If the file is open, finalize the header and close it
        if (this->fileStream.is_open())
        {
            this->fileStream.flush();

            // Update the main chunk size and data sub-chunk size
            uint32_t dataChunkSize = static_cast<uint32_t>(this->sampleCount
                * this->sampleSize);
            uint32_t mainChunkSize = dataChunkSize + 36;
            this->fileStream.seekp(4);
            encode(this->fileStream, mainChunkSize);
            this->fileStream.seekp(40);
            encode(this->fileStream, dataChunkSize);

            this->fileStream.close();
        }
    }
}; /* namespace Voceliand */
