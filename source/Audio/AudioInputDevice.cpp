/*
 *  File:   AudioInputDevice.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-10
 */

/* standard library includes */
#include <cstdio>   /* fprintf(), stderr */
#include <cstring>  /* memset, memcpy */
#include <cstdlib>  /* exit() */

/* external library includes */
/* project includes */
#include "../../include/Audio/SoundIoInterface.hpp"
#include "../../include/Audio/AudioInputDevice.hpp"

namespace Voceliand
{
    /*
     *  Anonymous namespace to avoid polluting other compilation units.
     */
    namespace
    {
        enum SoundIoFormat prioritized_formats[] = {
            SoundIoFormatFloat32NE,
            SoundIoFormatFloat32FE,
            SoundIoFormatS32NE,
            SoundIoFormatS32FE,
            SoundIoFormatS24NE,
            SoundIoFormatS24FE,
            SoundIoFormatS16NE,
            SoundIoFormatS16FE,
            SoundIoFormatFloat64NE,
            SoundIoFormatFloat64FE,
            SoundIoFormatU32NE,
            SoundIoFormatU32FE,
            SoundIoFormatU24NE,
            SoundIoFormatU24FE,
            SoundIoFormatU16NE,
            SoundIoFormatU16FE,
            SoundIoFormatS8,
            SoundIoFormatU8,
            SoundIoFormatInvalid,
        };

        int prioritized_sample_rates[] = {
            44100,
            48000,
            96000,
            24000,
            0,
        };

        int minInt(int a, int b)
        {
            return (a < b) ? a : b;
        }

        void readCallback(struct SoundIoInStream * instream,
            int frameCountMin, int frameCountMax)
        {
            struct RecordContext * rc;
            struct SoundIoChannelArea * areas;
            int error;
            char * writePtr;
            int writeFrames;

            rc = (RecordContext *) instream->userdata;

            {
                int freeBytes;
                int freeCount;

                writePtr = soundio_ring_buffer_write_ptr(rc->ringBuffer);
                freeBytes = soundio_ring_buffer_free_count(rc->ringBuffer);
                freeCount = freeBytes / instream->bytes_per_frame;
                if (freeCount < frameCountMin)
                {
                    fprintf(stderr, "ring buffer overflow\n");
                    exit(1);
                }

                writeFrames = minInt(freeCount, frameCountMax);
            }

            for (int framesLeft = writeFrames; framesLeft > 0;)
            {
                int frameCount;

                frameCount = framesLeft;
                error = soundio_instream_begin_read(instream, &areas,
                    &frameCount);
                if (error)
                {
                    fprintf(stderr, "begin read error: %s",
                        soundio_strerror(error));
                    exit(1);
                }
                if (!frameCount)
                {
                    break;
                }

                if (!areas)
                {
                    // Due to an overflow there is a hole. Fill the ring buffer
                    // with silence for the size of the hole.
                    memset(writePtr, 0, frameCount * instream->bytes_per_frame);
                }
                else
                {
                    for (int frame = 0; frame < frameCount; frame++)
                    {
                        for (int ch = 0; ch < instream->layout.channel_count; ch++)
                        {
                            memcpy(writePtr, areas[ch].ptr,
                                instream->bytes_per_sample);
                            areas[ch].ptr += areas[ch].step;
                            writePtr += instream->bytes_per_sample;
                        }
                    }
                }

                if ((error = soundio_instream_end_read(instream)))
                {
                    fprintf(stderr, "end read error: %s",
                        soundio_strerror(error));
                    exit(1);
                }

                framesLeft -= frameCount;
                if (framesLeft <= 0)
                {
                    break;
                }
            }

            {
                int advanceBytes;

                advanceBytes = writeFrames * instream->bytes_per_frame;
                soundio_ring_buffer_advance_write_ptr(rc->ringBuffer,
                    advanceBytes);
            }
        }

        void overflowCallback(struct SoundIoInStream * instream)
        {
            (void) instream; /* TODO */
            static int count = 0;
            fprintf(stderr, "overflow %d\n", ++count);
        }
    };

    /* Constructor. Instanciate .device and .stream. */
    AudioInputDevice::AudioInputDevice(bool isRaw, char * deviceId)
    {
        #define AID_FAIL() \
        {\
            this->good = false;\
            return;\
        }
        #define AID_FAIL_ON(check) \
        {\
            if(check)\
            {\
                AID_FAIL();\
            }\
        }

        /* error checking variable */
        Status::Code status;

        this->device = NULL;
        this->stream = NULL;
        this->context.ringBuffer = NULL;
        this->good = true;

        /* select first device available in list */
        status = this->selectDevice(isRaw, deviceId);
        AID_FAIL_ON(status != Status::SUCCESS);

        /* ? */
        soundio_device_sort_channel_layouts(this->device);

        this->stream = soundio_instream_create(this->device);
        if (!this->stream)
        {
            fprintf(stderr, "out of memory\n");
            AID_FAIL();
        }

        /* select proper format */
        this->stream->format = retrieveFormat();
        /* select proper sample rate */
        this->stream->sample_rate = retrieveSampleRate();
        this->stream->read_callback = &readCallback;
        this->stream->overflow_callback = &overflowCallback;
        this->stream->userdata = &this->context;

        this->sampleSize = soundio_get_bytes_per_sample(this->stream->format);

        status = this->start();
        AID_FAIL_ON(status != Status::SUCCESS);

        #undef AID_FAIL_ON
        #undef AID_FAIL
    }

    /* Destructor. Frees up space used by the object. */
    AudioInputDevice::~AudioInputDevice(void)
    {
        if(this->stream)
        {
            soundio_instream_destroy(this->stream);
        }

        if(this->context.ringBuffer)
        {
            soundio_ring_buffer_destroy(this->context.ringBuffer);
        }

        if(this->device)
        {
            soundio_device_unref(this->device);
        }
    }

    /* Poll audio samples from stream and put them in samples. */
    void AudioInputDevice::pollSamples(std::vector<char> & samples)
    {
        int fillBytes;
        char * readBuffer;

        samples.clear(); /* -MEH- */

        fillBytes = soundio_ring_buffer_fill_count(this->context.ringBuffer);
        readBuffer = soundio_ring_buffer_read_ptr(this->context.ringBuffer);

        /* copy everything
         * -MEH-? */
        samples.insert(samples.end(), readBuffer, readBuffer + fillBytes);

        soundio_ring_buffer_advance_read_ptr(this->context.ringBuffer, fillBytes);
    }

    /* Select the device in the inside the SoundIo device list. */
    Status::Code AudioInputDevice::selectDevice(bool isRaw, char * deviceId)
    {
        struct SoundIo * soundio = SoundIoInterface::getInstance().soundio;

        if (deviceId)
        {
            for (int i = 0; i < soundio_input_device_count(soundio); i++)
            {
                struct SoundIoDevice * device;

                device = soundio_get_input_device(soundio, i);
                if (device->is_raw == isRaw && strcmp(device->id, deviceId) == 0)
                {
                    this->device = device;
                    break;
                }

                soundio_device_unref(device);
            }

            if (!this->device)
            {
                fprintf(stderr, "Invalid device id: %s\n", deviceId);
                return Status::FAILURE_INVALID_DEVICEID;
            }
        }
        else
        {
            int deviceIndex;

            deviceIndex = soundio_default_input_device_index(soundio);

            this->device = soundio_get_input_device(soundio, deviceIndex);
            if (!this->device)
            {
                fprintf(stderr, "No input devices available.\n");
                return Status::FAILURE_NO_INPUT_DEVICE_AVAILABLE;
            }
        }

        fprintf(stderr, "Device: %s\n", this->device->name);

        if (this->device->probe_error)
        {
            fprintf(stderr, "Unable to probe device: %s\n",
                soundio_strerror(this->device->probe_error));
            return Status::FAILURE_DEVICE_UNPROBABLE;
        }

        return Status::SUCCESS;
    }

    /* Choose the first possible sample rate in the given list. */
    int AudioInputDevice::retrieveSampleRate(void)
    {
        int sampleRate = 0;

        for (int * sampleRatePtr = prioritized_sample_rates;
            *sampleRatePtr;
            sampleRatePtr++)
        {
            if (soundio_device_supports_sample_rate(this->device, *sampleRatePtr))
            {
                sampleRate = *sampleRatePtr;
                break;
            }
        }

        if (!sampleRate)
        {
            sampleRate = this->device->sample_rates[0].max;
        }

        return sampleRate;
    }

    enum SoundIoFormat AudioInputDevice::retrieveFormat(void)
    {
        enum SoundIoFormat format = SoundIoFormatInvalid;

        for (enum SoundIoFormat * formatPtr = prioritized_formats;
            *formatPtr != SoundIoFormatInvalid; formatPtr++)
        {
            if (soundio_device_supports_format(this->device, *formatPtr))
            {
                format = *formatPtr;
                break;
            }
        }

        if (format == SoundIoFormatInvalid)
        {
            format = this->device->formats[0];
        }

        return format;
    }

    /* Start the audio captation on the device. */
    Status::Code AudioInputDevice::start(void)
    {
        int error;

        error = soundio_instream_open(this->stream);
        if (error)
        {
            fprintf(stderr, "unable to open input stream: %s",
                soundio_strerror(error));
            return Status::FAILURE_INPUT_STREAM_UNOPENABLE;
        }

        fprintf(stderr, "%s %dHz %s interleaved\n",
            this->stream->layout.name, this->stream->sample_rate,
            soundio_format_string(this->stream->format));

        {
            const int RING_BUFFER_DURATION = 30; /* in seconds */

            SoundIo * soundio = SoundIoInterface::getInstance().soundio;
            int capacity = RING_BUFFER_DURATION * this->stream->sample_rate
                * this->stream->bytes_per_frame;

            this->context.ringBuffer = soundio_ring_buffer_create(soundio,
                capacity);
            if (!this->context.ringBuffer)
            {
                fprintf(stderr, "out of memory\n");
                return Status::FAILURE_OUT_OF_MEMORY;
            }
        }

        error = soundio_instream_start(this->stream);
        if (error)
        {
            fprintf(stderr, "unable to start input device: %s",
                soundio_strerror(error));
            return Status::FAILURE_CANT_START_INPUT_DEVICE;
        }

        return Status::SUCCESS;
    }
}; /* namespace Voceliand */
